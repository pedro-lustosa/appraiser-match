<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="creator" content="This page was created by the brazilian company DR Estúdio &ndash; https://danielrothier.com">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">

    <base href="../../" target="_self">
    <title>Search Results</title>

    <link rel="icon" href="multimedia/images/extra-small/ss-realty-2b.jpg">
    <link rel="stylesheet" href="styles/search-results/search-results-min.css">

    <script defer src="scripts/search-results/search-results-min.js" type="module"></script>
  </head>
  <body>
    <header id="top-header" class="default">
      <div class="nav-container">
        <div class="content-container">
          <picture class="logo">
            <img src="multimedia/images/extra-small/ss-logo-2.png" alt="">
          </picture>
          <div class="links-container">
            <nav class="pages-menu">
              <ul class="pages-list">
                <li class="about current"><a href="#">Why Get Listed?</a></li>
                <li class="license"><a href="#license">Create My Page</a></li>
                <li class="schedule"><a href="#fee-schedule">Sign In</a></li>
                <li class="payment"><a href="#overview">About Us</a></li>
                <li class="contact"><a href="#top-footer">Contact Us</a></li>
              </ul>
            </nav>
            <nav class="contact-menu">
              <ul class="social-list">
                <li><a class="facebook" href="#"></a></li>
                <li><a class="linkedin" href="#"></a></li>
                <li><a class="instagram" href="#"></a></li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </header>
    <main id="top-content">
      <form id="search-panel" name="main-search">
        <div class="content-container">
          <header class="heading-container">
            <h1 class="title">Real Estate Appraisers in New Rochelle, Westchester, NY</h1>
          </header>
          <main class="search-options">
            <div class="inputs-container">
              <div class="inputs-set">
                <picture class="filter-icon">
                  <img src="multimedia/images/extra-small/ss-filter-1.png" alt="Filter">
                </picture>
                <label class="state">
                  <span class="title">State</span>
                  <select name="main-state">
                    <option value="New York">New York</option>
                    <option value="New York">New York</option>
                    <option value="New York">New York</option>
                    <option value="New York">New York</option>
                    <option value="New York">New York</option>
                  </select>
                </label>
                <label class="county">
                  <span class="title">County</span>
                  <select name="main-county">
                    <option value="Westchester">Westchester</option>
                    <option value="Westchester">Westchester</option>
                    <option value="Westchester">Westchester</option>
                    <option value="Westchester">Westchester</option>
                    <option value="Westchester">Westchester</option>
                  </select>
                </label>
                <label class="city">
                  <span class="title">City</span>
                  <select name="main-city">
                    <option value="New York">New Yorks</option>
                    <option value="New York">New Yorks</option>
                    <option value="New York">New Yorks</option>
                    <option value="New York">New Yorks</option>
                    <option value="New York">New Yorks</option>
                  </select>
                </label>
              </div>
              <div class="inputs-set">
                <picture class="sort-icon">
                  <img src="multimedia/images/extra-small/ss-sort-1.png" alt="Sort">
                </picture>
                <label class="type">
                  <span class="title">Type</span>
                  <select name="main-type">
                    <option value="All">All</option>
                    <option value="All">All</option>
                    <option value="All">All</option>
                  </select>
                </label>
                <label class="certifications">
                  <span class="title">Certifications</span>
                  <select name="main-certifications">
                    <option value="All">All</option>
                    <option value="All">All</option>
                    <option value="All">All</option>
                  </select>
                </label>
                <label class="distance">
                  <span class="title">Distance</span>
                  <select name="main-distance">
                    <option value="Radius of 20 km">Radius of 20 km</option>
                    <option value="Radius of 20 km">Radius of 20 km</option>
                    <option value="Radius of 20 km">Radius of 20 km</option>
                  </select>
                </label>
              </div>
            </div>
            <div class="display-type-container">
              <picture class="displayer boxes current" tabindex="0">
                <img src="multimedia/images/extra-small/ss-grids-1.png" alt="">
              </picture>
              <picture class="displayer rows" tabindex="0">
                <img src="multimedia/images/extra-small/ss-grids-2.png" alt="">
              </picture>
            </div>
          </main>
        </div>
      </form>
      <div id="search-output">
        <div class="content-container">
          <div class="boxes-set">
            <article class="search-entry">
              <header class="heading-container">
                <dl class="address-entry">
                  <dt>Address</dt>
                  <dd></dd>
                </dl>
                <h2 class="appraiser">Andrew P Mantovani</h2>
                <span class="company">Mantovani &amp; Lynch, Inc.</span>
              </header>
              <dl class="info-list">
                <div class="tel-set">
                  <div class="mobile">
                    <dt>Mobile</dt>
                    <dd>516-984-6206</dd>
                  </div>
                  <div class="office">
                    <dt>Office</dt>
                    <dd>718-823-8600</dd>
                  </div>
                </div>
                <div class="additional-info">
                  <div class="type">
                    <dt>Type</dt>
                    <dd>Residential and Commercial</dd>
                  </div>
                  <div class="certifications">
                    <dt>Certifications</dt>
                    <dd><abbr class="certificate">ERC</abbr>, <abbr class="certificate">FHA</abbr>, <abbr class="certificate">HUD</abbr></dd>
                  </div>
                </div>
              </dl>
              <button type="button" name="search-send-email">
                <span class="content">E-mail</span>
              </button>
            </article>
            <article class="search-entry">
              <header class="heading-container">
                <dl class="address-entry">
                  <dt>Address</dt>
                  <dd></dd>
                </dl>
                <h2 class="appraiser">Eldred P Carhart</h2>
                <span class="company">Eldred P. Carhart, Appraisals</span>
              </header>
              <dl class="info-list">
                <div class="tel-set">
                  <div class="office">
                    <dt>Office</dt>
                    <dd>845-476-2700</dd>
                  </div>
                </div>
                <div class="additional-info">
                  <div class="type">
                    <dt>Type</dt>
                    <dd>Commercial</dd>
                  </div>
                  <div class="certifications">
                    <dt>Certifications</dt>
                    <dd>None</dd>
                  </div>
                </div>
              </dl>
              <button type="button" name="search-send-email">
                <span class="content">E-mail</span>
              </button>
            </article>
            <article class="search-entry">
              <header class="heading-container">
                <dl class="address-entry">
                  <dt>Address</dt>
                  <dd></dd>
                </dl>
                <h2 class="appraiser">Simcha Diamant</h2>
                <span class="company">Appraisal-solutions</span>
              </header>
              <dl class="info-list">
                <div class="tel-set">
                  <div class="office">
                    <dt>Office</dt>
                    <dd>845-774-7071</dd>
                  </div>
                </div>
                <div class="additional-info">
                  <div class="type">
                    <dt>Type</dt>
                    <dd>Residential and Commercial</dd>
                  </div>
                  <div class="certifications">
                    <dt>Certifications</dt>
                    <dd><abbr class="certificate">HUD</abbr>, <abbr class="certificate">FHA</abbr>, <abbr class="certificate">VA</abbr>, <abbr class="certificate">ERC</abbr></dd>
                  </div>
                </div>
              </dl>
              <button type="button" name="search-send-email">
                <span class="content">E-mail</span>
              </button>
            </article>
            <article class="search-entry">
              <header class="heading-container">
                <dl class="address-entry">
                  <dt>Address</dt>
                  <dd></dd>
                </dl>
                <h2 class="appraiser">Mendy Goldstein</h2>
                <span class="company">Reliable Appraisers, Inc.</span>
              </header>
              <dl class="info-list">
                <div class="tel-set">
                  <div class="office">
                    <dt>Office</dt>
                    <dd>917-709-9566</dd>
                  </div>
                </div>
                <div class="additional-info">
                  <div class="type">
                    <dt>Type</dt>
                    <dd>Residential</dd>
                  </div>
                  <div class="certifications">
                    <dt>Certifications</dt>
                    <dd>None</dd>
                  </div>
                </div>
              </dl>
              <button type="button" name="search-send-email">
                <span class="content">E-mail</span>
              </button>
            </article>
            <article class="search-entry">
              <header class="heading-container">
                <dl class="address-entry">
                  <dt>Address</dt>
                  <dd></dd>
                </dl>
                <h2 class="appraiser">Diane Dufrene</h2>
                <span class="company">Das Appraisals</span>
              </header>
              <dl class="info-list">
                <div class="tel-set">
                  <div class="office">
                    <dt>Office</dt>
                    <dd>845-476-2700</dd>
                  </div>
                </div>
                <div class="additional-info">
                  <div class="type">
                    <dt>Type</dt>
                    <dd>Commercial</dd>
                  </div>
                  <div class="certifications">
                    <dt>Certifications</dt>
                    <dd><abbr class="certificate">HUD</abbr>, <abbr class="certificate">VA</abbr>, <abbr class="certificate">FHA</abbr></dd>
                  </div>
                </div>
              </dl>
              <button type="button" name="search-send-email">
                <span class="content">E-mail</span>
              </button>
            </article>
            <article class="search-entry">
              <header class="heading-container">
                <dl class="address-entry">
                  <dt>Address</dt>
                  <dd></dd>
                </dl>
                <h2 class="appraiser">Steven B Guerrera</h2>
                <span class="company">Guerrera Appraisals, Inc.</span>
              </header>
              <dl class="info-list">
                <div class="tel-set">
                  <div class="office">
                    <dt>Office</dt>
                    <dd>631-563-8418</dd>
                  </div>
                </div>
                <div class="additional-info">
                  <div class="type">
                    <dt>Type</dt>
                    <dd>Residential and Commercial</dd>
                  </div>
                  <div class="certifications">
                    <dt>Certifications</dt>
                    <dd><abbr class="certificate">ERC</abbr>, <abbr class="certificate">FHA</abbr>, <abbr class="certificate">HUD</abbr></dd>
                  </div>
                </div>
              </dl>
              <button type="button" name="search-send-email">
                <span class="content">E-mail</span>
              </button>
            </article>
            <article class="search-entry">
              <header class="heading-container">
                <dl class="address-entry">
                  <dt>Address</dt>
                  <dd></dd>
                </dl>
                <h2 class="appraiser">Andrew P Mantovani</h2>
                <span class="company">Mantovani &amp; Lynch, Inc.</span>
              </header>
              <dl class="info-list">
                <div class="tel-set">
                  <div class="mobile">
                    <dt>Mobile</dt>
                    <dd>516-984-6206</dd>
                  </div>
                  <div class="office">
                    <dt>Office</dt>
                    <dd>718-823-8600</dd>
                  </div>
                </div>
                <div class="additional-info">
                  <div class="type">
                    <dt>Type</dt>
                    <dd>Residential and Commercial</dd>
                  </div>
                  <div class="certifications">
                    <dt>Certifications</dt>
                    <dd><abbr class="certificate">ERC</abbr>, <abbr class="certificate">FHA</abbr>, <abbr class="certificate">HUD</abbr></dd>
                  </div>
                </div>
              </dl>
              <button type="button" name="search-send-email">
                <span class="content">E-mail</span>
              </button>
            </article>
            <article class="search-entry">
              <header class="heading-container">
                <dl class="address-entry">
                  <dt>Address</dt>
                  <dd></dd>
                </dl>
                <h2 class="appraiser">Eldred P Carhart</h2>
                <span class="company">Eldred P. Carhart, Appraisals</span>
              </header>
              <dl class="info-list">
                <div class="tel-set">
                  <div class="office">
                    <dt>Office</dt>
                    <dd>845-476-2700</dd>
                  </div>
                </div>
                <div class="additional-info">
                  <div class="type">
                    <dt>Type</dt>
                    <dd>Commercial</dd>
                  </div>
                  <div class="certifications">
                    <dt>Certifications</dt>
                    <dd>None</dd>
                  </div>
                </div>
              </dl>
              <button type="button" name="search-send-email">
                <span class="content">E-mail</span>
              </button>
            </article>
            <article class="search-entry">
              <header class="heading-container">
                <dl class="address-entry">
                  <dt>Address</dt>
                  <dd></dd>
                </dl>
                <h2 class="appraiser">Simcha Diamant</h2>
                <span class="company">Appraisal-solutions</span>
              </header>
              <dl class="info-list">
                <div class="tel-set">
                  <div class="office">
                    <dt>Office</dt>
                    <dd>845-774-7071</dd>
                  </div>
                </div>
                <div class="additional-info">
                  <div class="type">
                    <dt>Type</dt>
                    <dd>Residential and Commercial</dd>
                  </div>
                  <div class="certifications">
                    <dt>Certifications</dt>
                    <dd><abbr class="certificate">HUD</abbr>, <abbr class="certificate">FHA</abbr>, <abbr class="certificate">VA</abbr>, <abbr class="certificate">ERC</abbr></dd>
                  </div>
                </div>
              </dl>
              <button type="button" name="search-send-email">
                <span class="content">E-mail</span>
              </button>
            </article>
            <article class="search-entry">
              <header class="heading-container">
                <dl class="address-entry">
                  <dt>Address</dt>
                  <dd></dd>
                </dl>
                <h2 class="appraiser">Mendy Goldstein</h2>
                <span class="company">Reliable Appraisers, Inc.</span>
              </header>
              <dl class="info-list">
                <div class="tel-set">
                  <div class="office">
                    <dt>Office</dt>
                    <dd>917-709-9566</dd>
                  </div>
                </div>
                <div class="additional-info">
                  <div class="type">
                    <dt>Type</dt>
                    <dd>Residential</dd>
                  </div>
                  <div class="certifications">
                    <dt>Certifications</dt>
                    <dd>None</dd>
                  </div>
                </div>
              </dl>
              <button type="button" name="search-send-email">
                <span class="content">E-mail</span>
              </button>
            </article>
            <article class="search-entry">
              <header class="heading-container">
                <dl class="address-entry">
                  <dt>Address</dt>
                  <dd></dd>
                </dl>
                <h2 class="appraiser">Diane Dufrene</h2>
                <span class="company">Das Appraisals</span>
              </header>
              <dl class="info-list">
                <div class="tel-set">
                  <div class="office">
                    <dt>Office</dt>
                    <dd>845-476-2700</dd>
                  </div>
                </div>
                <div class="additional-info">
                  <div class="type">
                    <dt>Type</dt>
                    <dd>Commercial</dd>
                  </div>
                  <div class="certifications">
                    <dt>Certifications</dt>
                    <dd><abbr class="certificate">HUD</abbr>, <abbr class="certificate">VA</abbr>, <abbr class="certificate">FHA</abbr></dd>
                  </div>
                </div>
              </dl>
              <button type="button" name="search-send-email">
                <span class="content">E-mail</span>
              </button>
            </article>
            <article class="search-entry">
              <header class="heading-container">
                <dl class="address-entry">
                  <dt>Address</dt>
                  <dd></dd>
                </dl>
                <h2 class="appraiser">Steven B Guerrera</h2>
                <span class="company">Guerrera Appraisals, Inc.</span>
              </header>
              <dl class="info-list">
                <div class="tel-set">
                  <div class="office">
                    <dt>Office</dt>
                    <dd>631-563-8418</dd>
                  </div>
                </div>
                <div class="additional-info">
                  <div class="type">
                    <dt>Type</dt>
                    <dd>Residential and Commercial</dd>
                  </div>
                  <div class="certifications">
                    <dt>Certifications</dt>
                    <dd><abbr class="certificate">ERC</abbr>, <abbr class="certificate">FHA</abbr>, <abbr class="certificate">HUD</abbr></dd>
                  </div>
                </div>
              </dl>
              <button type="button" name="search-send-email">
                <span class="content">E-mail</span>
              </button>
            </article>
          </div>
          <nav class="results-nav">
            <button type="button" name="previous">Back</button>
            <div class="pages-info">
              <span class="description">
                <span class="current-page">1</span>&sol;<span class="total-pages">2</span> Pages
              </span>
            </div>
            <button type="button" name="next">Next</button>
          </nav>
        </div>
      </div>
    </main>
    <footer id="top-footer">
      <section class="contact-info">
        <div class="content-container">
          <div class="side-info">
            <nav class="links-container">
              <picture class="logo">
                <img src="multimedia/images/extra-small/ss-logo-3.png" alt="Logo">
              </picture>
              <ul class="social-list">
                <li><a class="facebook" href="#"></a></li>
                <li><a class="linkedin" href="#"></a></li>
                <li><a class="instagram" href="#"></a></li>
              </ul>
            </nav>
            <span class="message">
              e.g. Modern townhouse | Image &copy; Peter Alfred Hess<span class="removable"> / CC by 2.0</span>
            </span>
          </div>
          <form name="footer-contact" action="" method="post">
            <header class="heading-container">
              <h1 class="title">Contact Now</h1>
            </header>
            <main class="form-body">
              <input name="footer-name" type="text" placeholder="Your Name" required>
              <div class="input-set">
                <input name="footer-email" type="email" placeholder="Your Email" required>
                <input name="footer-tel" type="tel" placeholder="Your Phone">
              </div>
              <input type="text" name="footer-address" placeholder="Property Address">
              <textarea name="footer-message" rows="4" cols="80" placeholder="Type your message..." required></textarea>
              <div class="buttons-set">
                <div class="boot-finder">
                  <div class="code-container">
                    <span class="code">KJH234</span>
                    <span class="reset">&orarr;</span>
                  </div>
                  <input type="text" name="footer-boot-finder" placeholder="Type the text" required>
                </div>
                <button type="submit" name="footer-contact-info">
                  <span class="content">Send</span>
                </button>
              </div>
            </main>
          </form>
        </div>
      </section>
      <section class="copyright">
        <div class="content-container">
          <nav class="copyright-links">
            <ul class="pages-list">
              <li><a href="#">Terms of Use</a></li>
              <li><a href="#">Privacy Policy</a></li>
            </ul>
          </nav>
          <div class="copyright-info">
            <span class="message">© 2001-2018 AppraiserMatch.com<span class="removable"> - All Rights Reserved</span></span>
          </div>
        </div>
      </section>
    </footer>
  </body>
</html>
