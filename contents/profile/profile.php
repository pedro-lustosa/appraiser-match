<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="creator" content="This page was created by the brazilian company DR Estúdio &ndash; https://danielrothier.com">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">

    <base href="../../" target="_self">
    <title>Profile</title>

    <link rel="icon" href="multimedia/images/extra-small/ss-realty-2b.jpg">
    <link rel="stylesheet" href="styles/profile/profile-min.css">

    <script defer src="scripts/profile/profile-min.js" type="module"></script>
  </head>
  <body>
    <header id="top-header" class="profile">
      <div class="nav-container">
        <div class="content-container">
          <picture class="icon">
            <img src="multimedia/images/extra-small/ss-realty-1.png" alt="">
          </picture>
          <div class="links-container">
            <nav class="pages-menu">
              <ul class="pages-list">
                <li class="about current"><a href="#">About</a></li>
                <li class="license"><a href="#license">License</a></li>
                <li class="schedule"><a href="#fee-schedule">Fee Schedule</a></li>
                <li class="payment"><a href="#overview">Payment</a></li>
                <li class="contact"><a href="#top-footer">Contact Me</a></li>
              </ul>
            </nav>
            <nav class="contact-menu">
              <address class="contact-info">
                <p class="message">
                  <span class="action">Call Now</span>
                  <span class="tel">800 630 1790</span>
                </p>
              </address>
              <ul class="social-list">
                <li><a class="facebook" href="#"></a></li>
                <li><a class="linkedin" href="#"></a></li>
                <li><a class="instagram" href="#"></a></li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
      <div class="display-heading">
        <div class="content-container">
          <div class="text-container">
            <hgroup class="heading-container">
              <h2 class="subtitle">Residential and Commercial Real Estate Appraiser</h2>
              <h1 class="title">Ken Flowers</h1>
            </hgroup>
            <ul class="info-list">
              <li>
                <figure class="image-container">
                  <picture class="ilustration">
                    <img src="multimedia/images/extra-small/ss-realty-2c.png" alt="">
                  </picture>
                  <figcaption class="description-container">
                    <h1 class="description">Residential &amp; Commercial</h1>
                  </figcaption>
                </figure>
              </li>
              <li>
                <figure class="image-container">
                  <picture class="ilustration">
                    <img src="multimedia/images/extra-small/ss-certificate-1b.png" alt="">
                  </picture>
                  <figcaption class="description-container">
                    <h1 class="description">Active License</h1>
                  </figcaption>
                </figure>
              </li>
              <li>
                <figure class="image-container">
                  <picture class="ilustration">
                    <img src="multimedia/images/extra-small/ss-realty-3b.png" alt="">
                  </picture>
                  <figcaption class="description-container">
                    <h1 class="description">15 years of experience</h1>
                  </figcaption>
                </figure>
              </li>
            </ul>
          </div>
          <div class="form-container">
            <form name="header-contact" action="" method="post">
              <header class="heading-container">
                <h1 class="title">Contact Now</h1>
                <dl class="response-time">
                  <dt>Avg Response Time</dt>
                  <dd>2 days or less</dd>
                </dl>
              </header>
              <main class="form-body">
                <input name="header-name" type="text" placeholder="Your Name" required>
                <div class="input-set">
                  <input name="header-email" type="email" placeholder="Your Email" required>
                  <input name="header-tel" type="tel" placeholder="Your Phone">
                </div>
                <div class="bottom-container"></div>
                <textarea name="header-message" rows="4" placeholder="Type your message..." required></textarea>
                <div class="buttons-set">
                  <div class="boot-finder">
                    <div class="code-container">
                      <span class="code">KJH234</span>
                      <span class="reset">&orarr;</span>
                    </div>
                    <input type="text" name="header-boot-finder" placeholder="Type the text" required>
                  </div>
                  <button type="submit" name="header-contact-info">
                    <span class="content">Send</span>
                  </button>
                </div>
              </main>
              <footer class="contact-info">
                <address class="tel">
                  <p class="action">Or <strong class="highlight">Call Now</strong> <span class="addendum">&lpar;Toll Free&rpar;</span>:</p>
                  <span class="tel-number">800 630 1790</span>
                </address>
              </footer>
            </form>
            <div class="form-backface">
              <div class="content-container">
                <div class="text-set">
                  <p class="message">Your message has been sent <b class="highlight">successfully</b>!</p>
                  <p class="message"><strong class="assertion">I will contact you as soon as possible.</strong></p>
                </div>
                <button type="button" name="header-success">
                  <span class="content">Ok</span>
                </button>
              </div>
            </div>
          </div>
          <div class="last-login-box">
            <dl class="last-login">
              <dt>Last login</dt>
              <dd>2 days ago</dd>
            </dl>
          </div>
        </div>
      </div>
    </header>
    <main id="top-content">
      <section id="profile-info">
        <h2 hidden>Profile Info</h2>
        <section id="about">
          <div class="content-container">
            <picture class="side-image">
              <img src="multimedia/images/small/s-person-1.jpg" alt="">
            </picture>
            <div class="side-text">
              <header class="heading-container">
                <hgroup class="heading-set">
                  <h3 class="title">About</h3>
                  <h4 class="subtitle">
                    A Commercial and Residential Real Estate Appraiser With 15 Years of Experience
                  </h4>
                </hgroup>
              </header>
              <div class="text-set">
                <p class="text">
                  <strong class="highlight">Nulla dignissim aspernatur erat molestie viverra dictum ridiculus iusto proident eleifend dicta mi lobortis eget</strong> aliquet nam luctus, commodi, doloremque, purus, commodi nonummy sagittis. Asperiores nec!
                  Officiis dignissim lacus lacus.
                </p>
                <p class="text">
                  Maiores diam morbi qui nostra adipisci vestibulum! Maiores hymenaeos, dignissim! Ac fringilla, porta, dui torquent parturient, nesciunt sit pariatur!
                  Mollit vitae doloribus massa dictumst nobis dicta, felis reiciendis tenetur lectus.
                </p>
                <p class="text">
                  Condimentum cubilia leo quia cursus? Laborum! Nihil aspernatur nunc scelerisque hic consectetuer sem. Error. Lectus cubilia, luctus ultricies fusce penatibus expedita.
                </p>
              </div>
            </div>
          </div>
        </section>
        <section id="overview">
          <div class="content-container">
            <header class="heading-container">
              <h4 class="title">Overview</h4>
            </header>
            <dl class="overview-data">
              <div class="data-set licenser-title">
                <dt>Title</dt>
                <dd><b class="bold">Owner/Appraiser</b></dd>
              </div>
              <div class="data-set specialties">
                <dt>Specialties</dt>
                <dd>Divorce/Estate</dd>
                <dd>Court Testimony</dd>
              </div>
              <div class="data-set certifications">
                <dt>Certifications</dt>
                <div class="certifications-set">
                  <dd>
                    <div class="circle">
                      <span class="certification">ERC</span>
                    </div>
                  </dd>
                  <dd>
                    <div class="circle">
                      <span class="certification">FHA</span>
                    </div>
                  </dd>
                  <dd>
                    <div class="circle">
                      <span class="certification">HUD</span>
                    </div>
                  </dd>
                </div>
              </div>
              <div class="data-set appraisal-software">
                <dt>Appraisal Software</dt>
                <dd>Bradford/Clickforms</dd>
              </div>
              <div class="data-set organizations">
                <dt>Organizations/Memberships</dt>
                <dd>Member of the South Carolina Appraisers Coalition</dd>
                <dd>Member of the Appraisal Institute</dd>
              </div>
              <hr class="divisor">
              <div class="data-set designations">
                <dt>Designations Earned</dt>
                <dd>St.Cert.Res.</dd>
                <dd>REA RD3303 FHA/HUD Certified</dd>
              </div>
              <div class="data-set payment">
                <dt>Payment</dt>
                <div class="cards-block">
                  <span class="types">Types Accepted</span>
                  <div class="cards-set">
                    <dd>
                      <picture class="visa">
                        <img src="multimedia/images/extra-small/ss-payment-card-5.png" alt="Visa">
                      </picture>
                    </dd>
                    <dd>
                      <picture class="mastercard">
                        <img src="multimedia/images/extra-small/ss-payment-card-2.png" alt="Mastercard">
                      </picture>
                    </dd>
                    <dd>
                      <picture class="discover">
                        <img src="multimedia/images/extra-small/ss-payment-card-1.png" alt="Discover">
                      </picture>
                    </dd>
                    <dd>
                      <picture class="paypal">
                        <img src="multimedia/images/extra-small/ss-payment-card-3.png" alt="PayPal">
                      </picture>
                    </dd>
                    <dd>
                      <picture class="amex">
                        <img src="multimedia/images/extra-small/ss-payment-card-4.png" alt="Amex">
                      </picture>
                    </dd>
                  </div>
                </div>
              </div>
            </dl>
          </div>
        </section>
        <section id="company">
          <div class="content-container">
            <header class="heading-container">
              <h4 class="title">Company</h4>
            </header>
            <dl class="company-data">
              <div class="data-set company">
                <dt>Company</dt>
                <dd class="highlight">
                  <span class="line">Alliance Appraisal</span>
                  <span class="line">Group, LLC</span>
                </dd>
              </div>
              <div class="data-set company-type">
                <dt>Company Type</dt>
                <dd>
                  <span class="line">Limited Liability</span>
                  <span class="line">Corporation &lpar;LLC&rpar;</span>
                </dd>
              </div>
              <div class="data-set address">
                <dt>Address</dt>
                <dd class="highlight">
                  <span class="line">106 North Edisto Drive,</span>
                  <span class="line">Florence, SC, 29501</span>
                </dd>
              </div>
              <div class="data-set contact-toll-free">
                <dt>Toll Free</dt>
                <dd class="highlight">800 630 1790</dd>
              </div>
              <div class="data-set contact-mobile">
                <dt>Mobile</dt>
                <dd class="highlight">843 230 6699</dd>
              </div>
              <div class="data-set contact-office">
                <dt>Office&sol;Fax</dt>
                <dd class="highlight">800 630 1790</dd>
              </div>
              <hr class="divisor">
              <div class="data-set deliverables">
                <dt>Deliverables</dt>
                <dd>
                  <ul class="deliverables-list">
                    <li>Comparable Photos</li>
                    <li>Building Sketch (if applicable)</li>
                    <li>Flood map (where applicable)</li>
                    <li>Subject Photos</li>
                    <li>Plat Map (if applicable)</li>
                    <li>Area/Location Map</li>
                    <li>Additional Addenda: Forms/Maps/Certifications (as required)</li>
                    <li>Appropriate form</li>
                  </ul>
                </dd>
              </div>
              <div class="data-set file-formats">
                <dt>File Formats</dt>
                <dd>
                  <ul class="files-list">
                    <li class="pdf">
                      <div class="content-set">
                        <picture class="file-icon">
                          <img src="multimedia/images/extra-small/ss-pdf-1.png" alt="">
                        </picture>
                        <span class="file-name">PDF</span>
                      </div>
                    </li>
                    <li class="edi">
                      <div class="content-set">
                        <picture class="file-icon">
                          <img src="multimedia/images/extra-small/ss-file-1.png" alt="">
                        </picture>
                        <span class="file-name">EDI</span>
                      </div>
                    </li>
                    <li class="xml">
                      <div class="content-set">
                        <picture class="file-icon">
                          <img src="multimedia/images/extra-small/ss-xml-1.png" alt="">
                        </picture>
                        <span class="file-name">XML</span>
                      </div>
                    </li>
                    <li class="alamode">
                      <div class="content-set">
                        <picture class="file-icon">
                          <img src="multimedia/images/extra-small/ss-file-1.png" alt="">
                        </picture>
                        <span class="file-name">Alamode</span>
                      </div>
                    </li>
                  </ul>
                </dd>
              </div>
            </dl>
          </div>
        </section>
        <footer id="profile-footer">
          <div class="content-container">
            <div class="downloads-container">
              <a href="" class="resume">
                Download Resume
              </a>
              <a href="" class="proof-of-insurance">
                Download Proof of Insurance
              </a>
            </div>
          </div>
        </footer>
        <section id="license">
          <div class="content-container">
            <header class="heading-container">
              <h3 class="title">License</h3>
            </header>
            <div class="licenses-set">
              <dl class="data-list">
                <div class="info-set main">
                  <header class="heading-block">
                    <div class="data-set">
                      <dt class="license">Number</dt>
                      <dd class="license">
                        <span class="number">5006</span>
                      </dd>
                    </div>
                    <div class="main-data">
                      <dt class="status">Status</dt>
                      <dd class="status">
                        <span class="key-info">Active License</span>
                        <div class="additional-info">
                          <span class="verified">Verified <time datetime="P21D">21 days ago</time></span>
                          <a href="http://www.asc.gov" class="source">www.asc.gov</a>
                        </div>
                      </dd>
                    </div>
                  </header>
                  <div class="side-data">
                    <div class="data-set">
                      <dt class="type">Type</dt>
                      <dd class="type">
                        Certified General
                      </dd>
                    </div>
                    <div class="data-set">
                      <dt class="expiration">Expiration Date</dt>
                      <dd class="expiration">
                        <time datetime="2018-06-30">June 30, 2018</time>
                      </dd>
                    </div>
                    <div class="data-set">
                      <dt class="state">State</dt>
                      <dd class="state">
                        <abbr>SC</abbr>
                      </dd>
                    </div>
                  </div>
                </div>
                <div class="info-set side">
                  <div class="data-set">
                    <dt class="counties">Counties</dt>
                    <dd class="counties">
                      <ul class="cities-list">
                        <li>Marion</li>
                        <li>Williamsburg</li>
                        <li>Lee</li>
                        <li>Florence</li>
                        <li>Dillon</li>
                        <li>Modoc</li>
                        <li>Marlboro</li>
                        <li>El Dorado</li>
                        <li>Marin</li>
                        <li>Santa Clara</li>
                        <li>Darlington</li>
                        <li>Butte</li>
                        <li>Mendocino</li>
                        <li>Amador</li>
                        <li>Fresno County</li>
                        <li>Tulare</li>
                        <li>County</li>
                        <li>Placer</li>
                        <li>Monterey</li>
                        <li>Tehama</li>
                        <li>San Mateo</li>
                      </ul>
                    </dd>
                  </div>
                  <div class="data-set">
                    <dt class="cities-excluded">Cities Excluded</dt>
                    <dd class="cities-excluded">
                      <ul class="cities-list">
                        <li>Humboldt</li>
                        <li>Plumas</li>
                        <li>San Luis</li>
                        <li>Obispo County</li>
                        <li>Kern County</li>
                        <li>Shasta</li>
                        <li>San Joaquin</li>
                        <li>Los Angeles</li>
                        <li>County</li>
                        <li>Johnsonville</li>
                        <li>Pamplico</li>
                        <li>Olanta</li>
                        <li>Lake City</li>
                        <li>Scranton</li>
                        <li>Coward</li>
                        <li>Timmonsville</li>
                      </ul>
                    </dd>
                  </div>
                </div>
              </dl>
              <dl class="data-list">
                <div class="info-set main">
                  <header class="heading-block">
                    <div class="data-set">
                      <dt class="license">Number</dt>
                      <dd class="license">
                        <span class="number">RD3303</span>
                      </dd>
                    </div>
                    <div class="main-data">
                      <dt class="status">Status</dt>
                      <dd class="status">
                        <span class="key-info">Active License</span>
                        <div class="additional-info">
                          <span class="verified">Verified <time datetime="P21D">21 days ago</time></span>
                          <a href="http://www.asc.gov" class="source">www.asc.gov</a>
                        </div>
                      </dd>
                    </div>
                  </header>
                  <div class="side-data">
                    <div class="data-set">
                      <dt class="type">Type</dt>
                      <dd class="type">
                        Certified General
                      </dd>
                    </div>
                    <div class="data-set">
                      <dt class="expiration">Expiration Date</dt>
                      <dd class="expiration">
                        <time datetime="2018-06-30">June 30, 2018</time>
                      </dd>
                    </div>
                    <div class="data-set">
                      <dt class="state">State</dt>
                      <dd class="state">
                        <abbr>SC</abbr>
                      </dd>
                    </div>
                  </div>
                </div>
                <div class="info-set side">
                  <div class="data-set">
                    <dt class="counties">Counties</dt>
                    <dd class="counties">
                      <ul class="cities-list">
                        <li>San Bernardino County</li>
                        <li>Merced County</li>
                        <li>Santa Cruz</li>
                        <li>Tuolumne</li>
                        <li>Sutter</li>
                        <li>San Diego</li>
                        <li>County</li>
                        <li>Inyo</li>
                        <li>Madera</li>
                        <li>Kings</li>
                        <li>Stanislaus</li>
                        <li>Colusa</li>
                        <li>Imperial County</li>
                        <li>Sacramento</li>
                        <li>County</li>
                        <li>Trinity</li>
                        <li>Nevada</li>
                      </ul>
                    </dd>
                  </div>
                  <div class="data-set">
                    <dt class="cities-excluded">Cities Excluded</dt>
                    <dd class="cities-excluded">
                      <ul class="cities-list">
                        <li>Humboldt</li>
                        <li>Plumas</li>
                        <li>San Luis</li>
                        <li>Obispo County</li>
                        <li>Kern County</li>
                        <li>Shasta</li>
                        <li>San Joaquin</li>
                        <li>Los Angeles</li>
                        <li>County</li>
                        <li>Johnsonville</li>
                        <li>Pamplico</li>
                        <li>Olanta</li>
                        <li>Lake City</li>
                        <li>Scranton</li>
                        <li>Coward</li>
                        <li>Timmonsville</li>
                      </ul>
                    </dd>
                  </div>
                </div>
              </dl>
            </div>
          </div>
        </section>
      </section>
      <section id="fee-schedule">
        <div class="content-container">
          <header class="heading-container">
            <h2 class="title">Fee Schedule</h2>
          </header>
          <ul class="display-options">
            <li class="current" tabindex="0">Apraisal Fee Schedule</li>
            <li tabindex="0">Miscellaneous<span class="removable"> &amp; Delivery Fees</span></li>
          </ul>
          <table class="schedule-table">
            <thead>
              <tr>
                <th class="form">Form</th>
                <th class="service">Service</th>
                <th class="fee">Fee<span class="removable"> &lpar;USD&rpar;</span></th>
                <th class="turnaround">Turnaround Time</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="form">1004 Full</td>
                <td class="service">Standard Single Family Residence</td>
                <td class="fee">$450.00 Minimum</td>
                <td class="turnaround">4 Days or less</td>
              </tr>
              <tr>
                <td class="form">1073 Full</td>
                <td class="service">Condominium &sol; Townhouse</td>
                <td class="fee">$450.00 Minimum</td>
                <td class="turnaround">4 Days or less</td>
              </tr>
              <tr>
                <td class="form">2055/2065</td>
                <td class="service">Drive By - Exterior only</td>
                <td class="fee">$350.00 Minimum</td>
                <td class="turnaround">3 Days or less</td>
              </tr>
              <tr>
                <td class="form">2055/2065</td>
                <td class="service">Interior and Exterior (no cost approach)</td>
                <td class="fee">$400.00 Minimum</td>
                <td class="turnaround">4 Days or less</td>
              </tr>
              <tr>
                <td class="form">2070/2075</td>
                <td class="service">Drive By - Exterior only (no value)</td>
                <td class="fee">$225.00 Minimum</td>
                <td class="turnaround">3 Days or less</td>
              </tr>
              <tr>
                <td class="form">2070/2075</td>
                <td class="service">Land Appraisals</td>
                <td class="fee">$300.00 Minimum</td>
                <td class="turnaround">3 Days or less</td>
              </tr>
              <tr>
                <td class="form">1025</td>
                <td class="service">Duplex, Triplex and Fourplex</td>
                <td class="fee">$650.00 Minimum</td>
                <td class="turnaround">5 Days or less</td>
              </tr>
              <tr>
                <td class="form">1032/2000</td>
                <td class="service">Field Review</td>
                <td class="fee">$400.00 Minimum</td>
                <td class="turnaround">4 Days or less</td>
              </tr>
              <tr>
                <td class="form">2006</td>
                <td class="service">Desk Review</td>
                <td class="fee">$250.00 Minimum</td>
                <td class="turnaround">3 Days or less</td>
              </tr>
              <tr>
                <td class="form">2006</td>
                <td class="service">Update of Appraisal (AKA: Re-certification)</td>
                <td class="fee">$150.00 Minimum</td>
                <td class="turnaround">3 Days or less</td>
              </tr>
            </tbody>
          </table>
          <nav class="table-nav">
            <button type="button" name="previous">Back</button>
            <div class="pages-info">
              <span class="description">
                <span class="current-page">1</span>&sol;<span class="total-pages">2</span> Pages
              </span>
            </div>
            <button type="button" name="next">Next</button>
          </nav>
        </div>
      </section>
    </main>
    <footer id="top-footer">
      <section class="contact-info">
        <div class="content-container">
          <form name="footer-contact" action="" method="post">
            <header class="heading-container">
              <h1 class="title">Contact <span class="user">Ken</span></h1>
              <dl class="response-time">
                <dt>Avg Response Time</dt>
                <dd>2 days or less</dd>
              </dl>
            </header>
            <main class="form-body">
              <input name="footer-name" type="text" placeholder="Your Name" required>
              <div class="input-set">
                <input name="footer-email" type="email" placeholder="Your Email" required>
                <input name="footer-tel" type="tel" placeholder="Your Phone">
              </div>
              <input type="text" name="footer-address" placeholder="Property Address">
              <textarea name="footer-message" rows="4" cols="80" placeholder="Type your message..." required></textarea>
              <div class="buttons-set">
                <div class="boot-finder">
                  <div class="code-container">
                    <span class="code">KJH234</span>
                    <span class="reset">&orarr;</span>
                  </div>
                  <input type="text" name="footer-boot-finder" placeholder="Type the text" required>
                </div>
                <button type="submit" name="footer-contact-info">
                  <span class="content">Send</span>
                </button>
              </div>
            </main>
            <footer class="contact-footer">
              <address class="tel-container">
                <span class="action">
                  <span class="main-message">Or Call Now</span>
                  <span class="addendum">&lpar;Toll Free&rpar;:</span>
                </span>
                <span class="tel">800 630 1790</span>
              </address>
              <nav class="links-container">
                <ul class="social-list">
                  <li><a class="facebook" href="#"></a></li>
                  <li><a class="linkedin" href="#"></a></li>
                  <li><a class="instagram" href="#"></a></li>
                </ul>
              </nav>
            </footer>
          </form>
        </div>
      </section>
      <section class="copyright">
        <div class="content-container">
          <div class="general-info">
            <picture class="logo">
              <img src="multimedia/images/extra-small/ss-logo-1.png" alt="Logo">
            </picture>
            <nav class="links-container">
              <ul class="pages-list">
                <li><a href="#">Why Get Listed?</a></li>
                <li><a href="#">Get Listed</a></li>
                <li><a href="#">About Us</a></li>
                <li><a href="#">Contact Us</a></li>
              </ul>
            </nav>
          </div>
          <div class="copyright-info">
            <nav class="links-container">
              <ul class="pages-list">
                <li><a href="#">Terms of Use</a></li>
                <li><a href="#">Privacy Policy</a></li>
              </ul>
            </nav>
            <span class="message">© 2001-2018 AppraiserMatch.com<span class="removable"> - All Rights Reserved</span></span>
          </div>
        </div>
      </section>
    </footer>
  </body>
</html>
