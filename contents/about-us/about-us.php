<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="creator" content="This page was created by the brazilian company DR Estúdio &ndash; https://danielrothier.com">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">

    <base href="../../" target="_self">
    <title>About us</title>

    <link rel="icon" href="multimedia/images/extra-small/ss-realty-2b.jpg">
    <link rel="stylesheet" href="styles/about-us/about-us-min.css">

    <script defer src="scripts/about-us/about-us-min.js" type="module"></script>
  </head>
  <body>
    <header id="top-header" class="default">
      <div class="nav-container">
        <div class="content-container">
          <picture class="logo">
            <img src="multimedia/images/extra-small/ss-logo-2.png" alt="">
          </picture>
          <div class="links-container">
            <nav class="pages-menu">
              <ul class="pages-list">
                <li class="about current"><a href="#">Why Get Listed?</a></li>
                <li class="license"><a href="#license">Create My Page</a></li>
                <li class="schedule"><a href="#fee-schedule">Sign In</a></li>
                <li class="payment"><a href="#overview">About Us</a></li>
                <li class="contact"><a href="#top-footer">Contact Us</a></li>
              </ul>
            </nav>
            <nav class="contact-menu">
              <ul class="social-list">
                <li><a class="facebook" href="#"></a></li>
                <li><a class="linkedin" href="#"></a></li>
                <li><a class="instagram" href="#"></a></li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
      <div class="display-heading">
        <div class="content-container">
          <div class="heading-container">
            <h1 class="title">About Us</h1>
            <p class="description">AppraiserMatch is your source for the most complete, accurate and up-to-date information about appraisers.</p>
          </div>
          <div class="form-container">
            <form name="header-contact" action="" method="post">
              <header class="heading-container">
                <h1 class="title">Contact Now</h1>
                <dl class="response-time">
                  <dt>Avg Response Time</dt>
                  <dd>2 days or less</dd>
                </dl>
              </header>
              <main class="form-body">
                <input name="header-name" type="text" placeholder="Your Name" required>
                <div class="input-set">
                  <input name="header-email" type="email" placeholder="Your Email" required>
                  <input name="header-tel" type="tel" placeholder="Your Phone">
                </div>
                <div class="bottom-container"></div>
                <textarea name="header-message" rows="4" placeholder="Type your message..." required></textarea>
                <div class="buttons-set">
                  <div class="boot-finder">
                    <div class="code-container">
                      <span class="code">KJH234</span>
                      <span class="reset">&orarr;</span>
                    </div>
                    <input type="text" name="header-boot-finder" placeholder="Type the text" required>
                  </div>
                  <button type="submit" name="header-contact-info">
                    <span class="content">Send</span>
                  </button>
                </div>
              </main>
            </form>
            <div class="form-backface">
              <div class="content-container">
                <div class="text-set">
                  <p class="message">Your message has been sent <b class="highlight">successfully</b>!</p>
                  <p class="message"><strong class="assertion">We will contact you as soon as possible.</strong></p>
                </div>
                <button type="button" name="header-success">
                  <span class="content">Ok</span>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
    <main id="top-content">
      <section id="about">
        <div class="content-container">
          <div class="text-container">
            <picture class="side-image">
              <img src="multimedia/images/extra-small/ss-ilustration-2.png" alt="">
            </picture>
            <div class="text-body">
              <header class="heading-container">
                <h2 class="title">About</h2>
              </header>
              <div class="text-set">
                <p class="text">
                  We are your most complete, accurate and up-to-date source of information on appraisers.
                </p>
                <p class="text">
                  In order to meet this claim, ApppraiserMatch is committed to:
                </p>
              </div>
            </div>
          </div>
          <div class="side-info">
            <ul class="topics-list">
              <li>
                <picture class="icon">
                  <img src="multimedia/images/extra-small/ss-certificate-1.png" alt="">
                </picture>
                <p class="text">
                  Verifying each appraiser's license status weekly with the National Registry.
                </p>
              </li>
              <li>
                <picture class="icon">
                  <img src="multimedia/images/extra-small/ss-move-1.png" alt="">
                </picture>
                <p class="text">
                  Reminding appraisers to keep their profiles current if they have not logged in to the site recently.
                </p>
              </li>
              <li>
                <picture class="icon">
                  <img src="multimedia/images/extra-small/ss-prize-1.png" alt="">
                </picture>
                <p class="text">
                  Rewarding appraisers for completing all sections of their profiles by factoring in the completeness score into the display of search results.
                </p>
              </li>
            </ul>
          </div>
        </div>
      </section>
      <section id="info-source">
        <div class="content-container">
          <div class="text-container">
            <header class="heading-container">
              <h2 class="title">Where does our information come from?</h2>
            </header>
            <div class="text-set">
              <p class="text">
                Information in our directory is entered and maintained directly by real estate appraisers. We also verify the status of appraiser licenses on a monthly basis using the National Registry of Appraisers.
              </p>
            </div>
          </div>
          <picture class="side-image">
            <img src="multimedia/images/extra-small/ss-ilustration-1.png" alt="">
          </picture>
        </div>
      </section>
    </main>
    <footer id="top-footer">
      <section class="contact-info">
        <div class="content-container">
          <div class="side-info">
            <nav class="links-container">
              <picture class="logo">
                <img src="multimedia/images/extra-small/ss-logo-3.png" alt="Logo">
              </picture>
              <ul class="social-list">
                <li><a class="facebook" href="#"></a></li>
                <li><a class="linkedin" href="#"></a></li>
                <li><a class="instagram" href="#"></a></li>
              </ul>
            </nav>
            <span class="message">
              e.g. Modern townhouse | Image &copy; Peter Alfred Hess<span class="removable"> / CC by 2.0</span>
            </span>
          </div>
          <form name="footer-contact" action="" method="post">
            <header class="heading-container">
              <h1 class="title">Contact Now</h1>
            </header>
            <main class="form-body">
              <input name="footer-name" type="text" placeholder="Your Name" required>
              <div class="input-set">
                <input name="footer-email" type="email" placeholder="Your Email" required>
                <input name="footer-tel" type="tel" placeholder="Your Phone">
              </div>
              <input type="text" name="footer-address" placeholder="Property Address">
              <textarea name="footer-message" rows="4" cols="80" placeholder="Type your message..." required></textarea>
              <div class="buttons-set">
                <div class="boot-finder">
                  <div class="code-container">
                    <span class="code">KJH234</span>
                    <span class="reset">&orarr;</span>
                  </div>
                  <input type="text" name="footer-boot-finder" placeholder="Type the text" required>
                </div>
                <button type="submit" name="footer-contact-info">
                  <span class="content">Send</span>
                </button>
              </div>
            </main>
          </form>
        </div>
      </section>
      <section class="copyright">
        <div class="content-container">
          <nav class="copyright-links">
            <ul class="pages-list">
              <li><a href="#">Terms of Use</a></li>
              <li><a href="#">Privacy Policy</a></li>
            </ul>
          </nav>
          <div class="copyright-info">
            <span class="message">© 2001-2018 AppraiserMatch.com<span class="removable"> - All Rights Reserved</span></span>
          </div>
        </div>
      </section>
    </footer>
  </body>
</html>
