"use stric";

/* Livrarias */
import "../global/libraries/theWheel/theWheel.js";

/* Configurações Gerais */
import "../global/site-presets/_functions.js";

/* Componentes */
import "../global/site-presets/top-header/_top-header.js";
import "./_search-whole.js";
import "../global/site-presets/top-footer/_top-footer.js";
