"use stric";

/* Livrarias */

import "../global/libraries/theWheel/theWheel-min.js";

/* Configurações Gerais */

Object.defineProperties( Array.prototype,
  { oFindTallest: // Encontra maior elemento em uma relação
      { value: function oFindTallest()
          { if( this.some( item => !( item instanceof HTMLElement ) ) ) throw new TypeError( "The target array must contain only instances of HTMLElement" );
            return this.slice().sort( ( a, b ) => b.offsetHeight - a.offsetHeight )[0] },
        enumerable: true } } );

Object.defineProperties( HTMLFormElement.prototype,
  { oPlaceAsterisc: // Adiciona um asterisco ao placeholder de campos obrigatórios de formulários
      { value: function oPlaceAsterisc()
          { let requiredInputs = Array.from( this.elements ).filter( input => input.placeholder && input.required );
            for( let input of requiredInputs ) input.placeholder += " *" },
        enumerable: true },
    oResetFields: // Simula uma redefinição dos campos de formulários
      { value: function oResetFields()
          { var inputsFields = Array.from( this.elements ).filter( input => input.tagName != "BUTTON" );
            for( let input of inputsFields ) input.value = "" },
        enumerable: true } } );

/* Componentes */

// Cabeçalho

( function ()
    { /* Identificadores */

      const topHeader = document.getElementById( "top-header" ); /* Cabeçalho Superior */
      topHeader:
        { var navBar = topHeader.querySelector( ".nav-container" ); /* Menu de Navegação */
          navBar:
            { var navContainer = navBar.container = navBar.querySelector( ".content-container" ), /* Recipiente de Conteúdo do Menu de Navegação */
                  pagesList = navBar.pagesList = navBar.querySelector( ".pages-list" ) /* Menu de Páginas do Site */ } }

      const media = { maxWidth: [ 1079, 1119 ] };

      /* Ouvintes de Evento */

      slidingMenu: // Implementa menu deslizante para versão responsiva
        { let targetMedia = topHeader.classList.contains( "profile" ) ? media.maxWidth[0] : media.maxWidth[1];
          adjustHeight: // Em telas de pouca largura, torna altura padrão da lista de páginas igual a de seu primeiro elemento
            { for( let _event of [ "load", "resize" ] )
                { window.addEventListener( _event, () =>
                  pagesList.oChangeBySize( { innerWidth: targetMedia },
                                           pagesList.oFlatSize.bind( pagesList, pagesList.firstElementChild, false ), () => pagesList.style.height = "" ) ) } }
          triggerSliding: // Em telas de pouca largura, ativa o deslizamento do menu
            { for( let _event of [ "load", "resize" ] )
                { window.addEventListener( _event, () =>
                    { pagesList.oChangeBySize( { innerWidth: targetMedia },
                                               () => { pagesList.setAttribute( "tabindex", 0 ) }, () => { pagesList.removeAttribute( "tabindex" ) } ) } ) };
              window.addEventListener( "resize", () => { if( window.innerWidth > targetMedia ) pagesList.classList.remove( "slided" ) } );
              pagesList.addEventListener( "click", () =>
                { if( window.innerWidth > targetMedia ) return;
                  pagesList.classList.toggle( "slided" ) ? pagesList.oEncompassChildren( false ) : pagesList.oFlatSize( pagesList.firstElementChild, false ) } )
              pagesList.addEventListener( "blur", () =>
                { if( pagesList.classList.contains( "slided" ) ) pagesList.click() } ) }
          adjustImgPlacement: // Ajustar posicionamento da imagem lateral do menu ao deslizamento da lista de páginas
            { pagesList.addEventListener( "click", () =>
                { if( window.innerWidth > targetMedia ) return;
                  if( pagesList.classList.contains( "slided" ) ) navContainer.style.alignItems = "flex-start" } );
              pagesList.addEventListener( "transitionend", event =>
                { if( window.innerWidth > targetMedia || event.propertyName != "height" ) return;
                  if( !pagesList.classList.contains( "slided" ) ) navContainer.style.alignItems = "" } ) } } } )();

// Seção de Busca
( function ()
    { /* Identificadores */

      const searchPanel = document.getElementById( "search-panel" ); // Painel de Procura
      searchPanel:
        { var displayerButtons = searchPanel.displayerButtons = searchPanel.querySelector( ".display-type-container" );
          displayerButtons:
            { var displayerButtonsType = displayerButtons.type = /* Botões que alternam aparência de exibição dos resultados da procura */
                { all: Array.from( displayerButtons.querySelectorAll( ".displayer" ) ) /* Todos os botões aplicáveis */,
                  boxes: displayerButtons.querySelector( ".boxes" ), /* Botão que retorna resultados em caixas */
                  rows: displayerButtons.querySelector( ".rows" ) /* Botão que retorna resultados em fileiras */ } } }

      const searchOutput = document.getElementById( "search-output" ); // Seção de Resultados da Procura
      searchOutput:
        { var resultsArea = searchOutput.resultsArea = searchOutput.querySelector( "[ class$='-set' ]" ) /* Área onde os resultados de pesquisa aparecem */ }

      /* Funções */

      const resetCurrentDisplayerButton = function( type ) // Redefine o botão atual de aparência dos resultados de pesquisa
        { let button = displayerButtonsType.all.find( button => button.classList.contains( type ) );
          if( button.classList.contains( "current" ) ) return;
          for( let type in displayerButtonsType )
            { let button = displayerButtonsType[ type ];
              if( Array.isArray( button ) || !button.classList.contains( "current" ) ) continue;
              button.classList.remove( "current" ); resultsArea.classList.remove( type + "-set" ) }
          button.classList.add( "current" ); resultsArea.classList.add( type + "-set" ) }

      const smallViewportSupport = function() // Assegura o uso do único estilo destinado à aparência dos resultados de pesquisa em telas de pouca largura
        { if( window.getComputedStyle( displayerButtons ).display == "none" ) resetCurrentDisplayerButton( "boxes" ) }

      /* Eventos */

      displayAppearanceControl: // Define a apresentação dos resultados de pesquisa
        { smallViewportSupport: // Assegura o uso do único estilo destinado à aparência dos resultados de pesquisa em telas de pouca largura
            { smallViewportSupport(); window.addEventListener( "resize", smallViewportSupport ) }
          for( let type in displayerButtonsType )
            { let button = displayerButtonsType[ type ]; if( Array.isArray( button ) ) continue;
              setEntriesAppearance: // Define a forma como os resultados de pesquisa aparecem
                { button.addEventListener( "click", () =>
                  { if( resultsArea.classList.contains( type + "-set" ) ) return;
                    for( let type in displayerButtonsType )
                      { if( resultsArea.classList.contains( type + "-set" ) ) { resultsArea.classList.remove( type + "-set" ); break } }
                    resultsArea.classList.add( type + "-set" ) } ) }
              setCurrentButton: // Estiliza o botão do painel de procura que no momento está definindo a aparência dos resultados de pesquisa
                { button.addEventListener( "click", () => button.oRestrictClass( "current", displayerButtonsType.all, true ) ) } } } } )();

// Rodapé
( function ()
    { /* Identificadores */

      const topFooter = document.getElementById( "top-footer" ); // Rodapé Superior
      topFooter:
        { var contactForm = topFooter.contactForm = topFooter.querySelector( "form[ name='footer-contact' ]" ) } /* Formulário de Contato Principal */

      /* Instruções Iniciais */

      addToPlaceholders: // Adiciona asterisco ao fim do conteúdo de placeholders de campos obrigatórios do formulário
        { contactForm.oPlaceAsterisc() } } )();
