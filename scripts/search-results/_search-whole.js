/* Identificadores */

const searchPanel = document.getElementById( "search-panel" ); // Painel de Procura
searchPanel:
  { var displayerButtons = searchPanel.displayerButtons = searchPanel.querySelector( ".display-type-container" );
    displayerButtons:
      { var displayerButtonsType = displayerButtons.type = /* Botões que alternam aparência de exibição dos resultados da procura */
          { all: Array.from( displayerButtons.querySelectorAll( ".displayer" ) ) /* Todos os botões aplicáveis */,
            boxes: displayerButtons.querySelector( ".boxes" ), /* Botão que retorna resultados em caixas */
            rows: displayerButtons.querySelector( ".rows" ) /* Botão que retorna resultados em fileiras */ } } }

const searchOutput = document.getElementById( "search-output" ); // Seção de Resultados da Procura
searchOutput:
  { var resultsArea = searchOutput.resultsArea = searchOutput.querySelector( "[ class$='-set' ]" ) /* Área onde os resultados de pesquisa aparecem */ }

/* Funções */

const resetCurrentDisplayerButton = function( type ) // Redefine o botão atual de aparência dos resultados de pesquisa
  { let button = displayerButtonsType.all.find( button => button.classList.contains( type ) );
    if( button.classList.contains( "current" ) ) return;
    for( let type in displayerButtonsType )
      { let button = displayerButtonsType[ type ];
        if( Array.isArray( button ) || !button.classList.contains( "current" ) ) continue;
        button.classList.remove( "current" ); resultsArea.classList.remove( type + "-set" ) }
    button.classList.add( "current" ); resultsArea.classList.add( type + "-set" ) }

const smallViewportSupport = function() // Assegura o uso do único estilo destinado à aparência dos resultados de pesquisa em telas de pouca largura
  { if( window.getComputedStyle( displayerButtons ).display == "none" ) resetCurrentDisplayerButton( "boxes" ) }

/* Eventos */

displayAppearanceControl: // Define a apresentação dos resultados de pesquisa
  { smallViewportSupport: // Assegura o uso do único estilo destinado à aparência dos resultados de pesquisa em telas de pouca largura
      { smallViewportSupport(); window.addEventListener( "resize", smallViewportSupport ) }
    for( let type in displayerButtonsType )
      { let button = displayerButtonsType[ type ]; if( Array.isArray( button ) ) continue;
        setEntriesAppearance: // Define a forma como os resultados de pesquisa aparecem
          { button.addEventListener( "click", () =>
            { if( resultsArea.classList.contains( type + "-set" ) ) return;
              for( let type in displayerButtonsType )
                { if( resultsArea.classList.contains( type + "-set" ) ) { resultsArea.classList.remove( type + "-set" ); break } }
              resultsArea.classList.add( type + "-set" ) } ) }
        setCurrentButton: // Estiliza o botão do painel de procura que no momento está definindo a aparência dos resultados de pesquisa
          { button.addEventListener( "click", () => button.oRestrictClass( "current", displayerButtonsType.all, true ) ) } } }
