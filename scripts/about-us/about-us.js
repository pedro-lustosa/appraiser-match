"use stric";

/* Livrarias */
import "../global/libraries/theWheel/theWheel.js";

/* Configurações Gerais */

Object.defineProperties( Array.prototype,
  { oFindTallest: // Encontra maior elemento em uma relação
      { value: function oFindTallest()
          { if( this.some( item => !( item instanceof HTMLElement ) ) ) throw new TypeError( "The target array must contain only instances of HTMLElement" );
            return this.slice().sort( ( a, b ) => b.offsetHeight - a.offsetHeight )[0] },
        enumerable: true } } );

Object.defineProperties( HTMLFormElement.prototype,
  { oPlaceAsterisc: // Adiciona um asterisco ao placeholder de campos obrigatórios de formulários
      { value: function oPlaceAsterisc()
          { let requiredInputs = Array.from( this.elements ).filter( input => input.placeholder && input.required );
            for( let input of requiredInputs ) input.placeholder += " *" },
        enumerable: true },
    oResetFields: // Simula uma redefinição dos campos de formulários
      { value: function oResetFields()
          { var inputsFields = Array.from( this.elements ).filter( input => input.tagName != "BUTTON" );
            for( let input of inputsFields ) input.value = "" },
        enumerable: true } } );

/* Componentes */

// Cabeçalho

( function ()
    { /* Identificadores */

      const topHeader = document.getElementById( "top-header" ); /* Cabeçalho Superior */
      topHeader:
        { var navBar = topHeader.querySelector( ".nav-container" ); /* Menu de Navegação */
          navBar:
            { var navContainer = navBar.container = navBar.querySelector( ".content-container" ), /* Recipiente de Conteúdo do Menu de Navegação */
                  pagesList = navBar.pagesList = navBar.querySelector( ".pages-list" ) /* Menu de Páginas do Site */ } }

      const media = { maxWidth: [ 1079, 1119 ] };

      /* Ouvintes de Evento */

      slidingMenu: // Implementa menu deslizante para versão responsiva
        { let targetMedia = topHeader.classList.contains( "profile" ) ? media.maxWidth[0] : media.maxWidth[1];
          adjustHeight: // Em telas de pouca largura, torna altura padrão da lista de páginas igual a de seu primeiro elemento
            { for( let _event of [ "load", "resize" ] )
                { window.addEventListener( _event, () =>
                  pagesList.oChangeBySize( { innerWidth: targetMedia },
                                           pagesList.oFlatSize.bind( pagesList, pagesList.firstElementChild, false ), () => pagesList.style.height = "" ) ) } }
          triggerSliding: // Em telas de pouca largura, ativa o deslizamento do menu
            { for( let _event of [ "load", "resize" ] )
                { window.addEventListener( _event, () =>
                    { pagesList.oChangeBySize( { innerWidth: targetMedia },
                                               () => { pagesList.setAttribute( "tabindex", 0 ) }, () => { pagesList.removeAttribute( "tabindex" ) } ) } ) };
              window.addEventListener( "resize", () => { if( window.innerWidth > targetMedia ) pagesList.classList.remove( "slided" ) } );
              pagesList.addEventListener( "click", () =>
                { if( window.innerWidth > targetMedia ) return;
                  pagesList.classList.toggle( "slided" ) ? pagesList.oEncompassChildren( false ) : pagesList.oFlatSize( pagesList.firstElementChild, false ) } )
              pagesList.addEventListener( "blur", () =>
                { if( pagesList.classList.contains( "slided" ) ) pagesList.click() } ) }
          adjustImgPlacement: // Ajustar posicionamento da imagem lateral do menu ao deslizamento da lista de páginas
            { pagesList.addEventListener( "click", () =>
                { if( window.innerWidth > targetMedia ) return;
                  if( pagesList.classList.contains( "slided" ) ) navContainer.style.alignItems = "flex-start" } );
              pagesList.addEventListener( "transitionend", event =>
                { if( window.innerWidth > targetMedia || event.propertyName != "height" ) return;
                  if( !pagesList.classList.contains( "slided" ) ) navContainer.style.alignItems = "" } ) } }

      /* Identificadores */

      const headerForm = topHeader.form = topHeader.querySelector( "form" ); // Formulário do cabeçalho
      headerForm:
        { var inputs =  Array.from( headerForm.elements ).filter( input => input.tagName == "INPUT" && !input.closest( ".buttons-set" ) ); /* Inputs relevantes do formulário */
          inputs:
            { var nameInput = inputs.leadName = inputs.find( input => input.name == "header-name" ), /* Campo para inserção do nome */
                  emailInput = inputs.leadEmail = inputs.find( input => input.name == "header-email" ) /* Campo para inserção do e-mail */ }
          var textArea = headerForm.textArea = headerForm.querySelector( "[ name='header-message' ]" ), /* Campo para inserção de mensagem */
              buttonsSet = headerForm.buttonsSet = headerForm.querySelector( ".buttons-set" ); /* Conjunto de botões */
          buttonsSet:
            { var submitButton = buttonsSet.submitButton = buttonsSet.querySelector( "[ type='submit' ]" ) /* Botão de submissão */ }
          var bottomContainer = headerForm.bottomContainer = headerForm.querySelector( ".bottom-container" ), /* Recipiente da parte inferior do formulário, somente usado na versão responsiva */
              formBackface = headerForm.backface = headerForm.parentElement.querySelector( ".form-backface" ); /* Face do formulário com notificação de submissão bem-sucedida */
          formBackface:
            { var successButton = formBackface.successButton = formBackface.querySelector( "[ name='header-success' ]" ) } }

      /* Instruções Iniciais */
      addToPlaceholders: // Adiciona asterisco ao fim do conteúdo de placeholders de campos obrigatórios do formulário
        { headerForm.oPlaceAsterisc() }

      responsiveConfig: // Altera composição do formulário em telas de pouca largura
        { let targetMedia = topHeader.classList.contains( "profile" ) ? media.maxWidth[0] : media.maxWidth[1];
          agroupInputs: // Ajunta os inputs do formulário em uma única div
            { nameInput.oChangePlacementBySize( { innerWidth: targetMedia }, emailInput.parentElement, emailInput );
              window.addEventListener( "resize", () => nameInput.oChangePlacementBySize( { innerWidth: targetMedia }, emailInput.parentElement, emailInput ) ) }
          bottomContainer: // Cria uma div para que agrupe o textarea e o grupo de botões
            { for( let element of [ textArea, buttonsSet ] )
                { element.oChangePlacementBySize( { innerWidth: targetMedia }, bottomContainer );
                  window.addEventListener( "resize", () => element.oChangePlacementBySize( { innerWidth: targetMedia }, bottomContainer ) ) } } }

      successfulSubmission: // Eventos a acontecerem após uma submissão bem-sucedida
        { headerForm.addEventListener( "submit", event => event.preventDefault() ); // Impedir que formulário recarregue por padrão a página
          changeToBackface: // Altera face do formulário para a que informa seu preenchimento bem-sucedido
            { headerForm.addEventListener( "submit", () =>
                { headerForm.oControlAnimation(); successButton.clicked = false } );
              headerForm.addEventListener( "animationiteration", () =>
                { headerForm.oControlAnimation();
                  if( !successButton.clicked ) formBackface.oControlAnimation() } );
              formBackface.addEventListener( "animationiteration", () =>
                { formBackface.oControlAnimation();
                  if( successButton.clicked ) headerForm.oControlAnimation() } );
              successButton.addEventListener( "click", () =>
                { formBackface.oControlAnimation(); successButton.clicked = true } ) }
          resetInputFields: // Redefinir campos de input quando se estiver a voltar para a face frontal do formulário
            { successButton.addEventListener( "click", () => headerForm.oResetFields() ) } } } )();

// Rodapé
( function ()
    { /* Identificadores */

      const topFooter = document.getElementById( "top-footer" ); // Rodapé Superior
      topFooter:
        { var contactForm = topFooter.contactForm = topFooter.querySelector( "form[ name='footer-contact' ]" ) } /* Formulário de Contato Principal */

      /* Instruções Iniciais */

      addToPlaceholders: // Adiciona asterisco ao fim do conteúdo de placeholders de campos obrigatórios do formulário
        { contactForm.oPlaceAsterisc() } } )();
