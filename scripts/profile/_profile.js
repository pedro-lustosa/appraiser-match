"use stric";

/* Livrarias */
import "../global/libraries/theWheel/theWheel.js";

/* Configurações Gerais */
import "../global/site-presets/_functions.js";

/* Componentes */
import "../global/site-presets/top-header/_top-header.js";
import "../global/site-presets/top-header/_header-form.js";
import "../global/site-presets/top-content/_overview.js";
import "../global/site-presets/top-content/_company.js";
import "../global/site-presets/top-content/_fee-schedule.js";
import "../global/site-presets/top-footer/_top-footer.js";
