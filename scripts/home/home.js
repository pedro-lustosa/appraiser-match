"use stric";

/* Livrarias */

import "../global/libraries/theWheel/theWheel-min.js";

/* Configurações Gerais */

Object.defineProperties( Array.prototype,
  { oFindTallest: // Encontra maior elemento em uma relação
      { value: function oFindTallest()
          { if( this.some( item => !( item instanceof HTMLElement ) ) ) throw new TypeError( "The target array must contain only instances of HTMLElement" );
            return this.slice().sort( ( a, b ) => b.offsetHeight - a.offsetHeight )[0] },
        enumerable: true } } );

Object.defineProperties( HTMLFormElement.prototype,
  { oPlaceAsterisc: // Adiciona um asterisco ao placeholder de campos obrigatórios de formulários
      { value: function oPlaceAsterisc()
          { let requiredInputs = Array.from( this.elements ).filter( input => input.placeholder && input.required );
            for( let input of requiredInputs ) input.placeholder += " *" },
        enumerable: true },
    oResetFields: // Simula uma redefinição dos campos de formulários
      { value: function oResetFields()
          { var inputsFields = Array.from( this.elements ).filter( input => input.tagName != "BUTTON" );
            for( let input of inputsFields ) input.value = "" },
        enumerable: true } } );

/* Componentes */

// Cabeçalho

( function ()
    { /* Identificadores */

      const topHeader = document.getElementById( "top-header" ); /* Cabeçalho Superior */
      topHeader:
        { var navBar = topHeader.querySelector( ".nav-container" ); /* Menu de Navegação */
          navBar:
            { var navContainer = navBar.container = navBar.querySelector( ".content-container" ), /* Recipiente de Conteúdo do Menu de Navegação */
                  pagesList = navBar.pagesList = navBar.querySelector( ".pages-list" ) /* Menu de Páginas do Site */ } }

      const media = { maxWidth: [ 1079, 1119 ] };

      /* Ouvintes de Evento */

      slidingMenu: // Implementa menu deslizante para versão responsiva
        { let targetMedia = topHeader.classList.contains( "profile" ) ? media.maxWidth[0] : media.maxWidth[1];
          adjustHeight: // Em telas de pouca largura, torna altura padrão da lista de páginas igual a de seu primeiro elemento
            { for( let _event of [ "load", "resize" ] )
                { window.addEventListener( _event, () =>
                  pagesList.oChangeBySize( { innerWidth: targetMedia },
                                           pagesList.oFlatSize.bind( pagesList, pagesList.firstElementChild, false ), () => pagesList.style.height = "" ) ) } }
          triggerSliding: // Em telas de pouca largura, ativa o deslizamento do menu
            { for( let _event of [ "load", "resize" ] )
                { window.addEventListener( _event, () =>
                    { pagesList.oChangeBySize( { innerWidth: targetMedia },
                                               () => { pagesList.setAttribute( "tabindex", 0 ) }, () => { pagesList.removeAttribute( "tabindex" ) } ) } ) };
              window.addEventListener( "resize", () => { if( window.innerWidth > targetMedia ) pagesList.classList.remove( "slided" ) } );
              pagesList.addEventListener( "click", () =>
                { if( window.innerWidth > targetMedia ) return;
                  pagesList.classList.toggle( "slided" ) ? pagesList.oEncompassChildren( false ) : pagesList.oFlatSize( pagesList.firstElementChild, false ) } )
              pagesList.addEventListener( "blur", () =>
                { if( pagesList.classList.contains( "slided" ) ) pagesList.click() } ) }
          adjustImgPlacement: // Ajustar posicionamento da imagem lateral do menu ao deslizamento da lista de páginas
            { pagesList.addEventListener( "click", () =>
                { if( window.innerWidth > targetMedia ) return;
                  if( pagesList.classList.contains( "slided" ) ) navContainer.style.alignItems = "flex-start" } );
              pagesList.addEventListener( "transitionend", event =>
                { if( window.innerWidth > targetMedia || event.propertyName != "height" ) return;
                  if( !pagesList.classList.contains( "slided" ) ) navContainer.style.alignItems = "" } ) } } } )();

// Seção de Dados Gerais de Licença

( function ()
    { /* Identificadores */

      const overview = document.getElementById( "overview" ); // Seção de Dados Gerais de Licença
      overview:
        { var dataList = overview.dataList = overview.querySelector( ".overview-data" );
          dataList:
            { var dataSets = dataList.dataSets = Array.from( dataList.getElementsByClassName( "data-set" ) ) } }

      /* Ouvintes de Evento */

      equalizeSetsHeight: // Tornar Igual a Altura dos Conjuntos de Data
        { window.addEventListener( "load", () =>
            { let tallestSet = dataSets.oFindTallest();
              for( let set of dataSets ) set.oFlatSize( tallestSet, false ) } ) } } )();

// Seção de Dados Gerais de Licença

( function ()
    { /* Identificadores */

      const company = document.getElementById( "company" ); // Seção de Dados Gerais de Licença
      company:
        { var dataList = company.dataList = company.querySelector( ".company-data" );
          dataList:
            { var dataSets = dataList.dataSets = Array.from( dataList.getElementsByClassName( "data-set" ) ) } }

      /* Ouvintes de Evento */

      equalizeSetsHeight: // Tornar Igual a Altura dos Conjuntos de Data
        { window.addEventListener( "load", () =>
            { let targetDataSets = dataSets.slice( 0, 6 ), tallestSet = targetDataSets.oFindTallest();
              for( let set of targetDataSets ) set.oFlatSize( tallestSet, false ) } ) } } )();

// Seção de Tabela de Tarifas

( function ()
    { /* Identificadores */

      const feeSchedule = document.getElementById( 'fee-schedule' ); // Seção de Tabela de Tarifas
      feeSchedule:
        { var displayerMenu = feeSchedule.displayerMenu = feeSchedule.querySelector( ".display-options" ); /* Menu de exibição dos dados da tabela */
          displayerMenu:
            { var menuOptions = displayerMenu.options = displayerMenu.querySelectorAll( "li" ) /* Opções do menu */ }
          var table = feeSchedule.table = feeSchedule.querySelector( ".schedule-table" ); /* Tabela de Tarifas */
          table:
            { var tData = table.data = table.querySelectorAll( "thead th" ); /* Células de Cabeçalho */
              data:
                { var fee = tData.fee = table.querySelectorAll( "tbody td:nth-of-type( 3 )" ), /* Valor da Tarifa */
                      turnaroundTime = tData.turnaroundTime = table.querySelectorAll( "tbody td:nth-of-type( 4 )" ); /* Tempo de Resposta */
                  turnaroundTime:
                    { var turnaroundHeading = turnaroundTime.heading = tData[ 3 ] /* Cabeçalho da Coluna de Tempo de Resposta */ } } } }

      const media = { maxWidth: [ 1119, 799 ] };

      /* Métodos */

      for( let td of fee )
        { td.compact = function()
            { if( this.responsiveText ) return this.responsiveText;
              var price = this.textContent.oFilterNumber();
              return this.responsiveText = `$${ price }+` } }

      for( let td of turnaroundTime )
          { td.compact = function()
              { if( this.responsiveText ) return this.responsiveText;
                var day = this.textContent.oFilterNumber();
                return this.responsiveText = `<= ${ day }D` } }

      turnaroundHeading.compact = function()
        { if( this.responsiveText ) return this.responsiveText;
          return this.responsiveText = "TAT" }

      /* Eventos */

      selectOptions: // Controla exibição de dados da tabela a partir da seleção das opções do menu
        { setCurrentOption: // Estiliza a opção do menu de que a tabela no momento está exibindo os dados
            { for( let option of menuOptions ) option.addEventListener( "click", () => option.oRestrictClass( "current", menuOptions, true ) ) } }

      compactText: // Tornar textos da tabela mais resumidos para versões responsivas
        { tbody:
            { for( let target of [ fee, turnaroundTime ] )
                { for( let td of target )
                    { td.oChangeBySize( { innerWidth: media.maxWidth[0] }, td.oChangeText.bind( td, td.compact() ) );
                      window.addEventListener( "resize", () =>
                        { td.oChangeBySize( { innerWidth: media.maxWidth[0] },
                                              td.oChangeText.bind( td, td.compact() ), td.oChangeText.bind( td, td.formerText || td.textContent ) ) } ) } } }
          thead:
            { let heading = turnaroundHeading;
              heading.oChangeBySize( { innerWidth: media.maxWidth[1] }, heading.oChangeText.bind( heading, heading.compact() ) );
              window.addEventListener( "resize", () => heading.oChangeBySize( { innerWidth: media.maxWidth[1] },
                                                       heading.oChangeText.bind( heading, heading.compact() ), heading.oChangeText.bind( heading, heading.formerText || heading.textContent ) ) ) } } } )();

// Rodapé
( function ()
    { /* Identificadores */

      const topFooter = document.getElementById( "top-footer" ); // Rodapé Superior
      topFooter:
        { var contactForm = topFooter.contactForm = topFooter.querySelector( "form[ name='footer-contact' ]" ) } /* Formulário de Contato Principal */

      /* Instruções Iniciais */

      addToPlaceholders: // Adiciona asterisco ao fim do conteúdo de placeholders de campos obrigatórios do formulário
        { contactForm.oPlaceAsterisc() } } )();
