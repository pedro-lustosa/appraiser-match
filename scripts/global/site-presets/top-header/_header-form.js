/* Identificadores */

import { topHeader, media } from "./_top-header.js";

const headerForm = topHeader.form = topHeader.querySelector( "form" ); // Formulário do cabeçalho
headerForm:
  { var inputs =  Array.from( headerForm.elements ).filter( input => input.tagName == "INPUT" && !input.closest( ".buttons-set" ) ); /* Inputs relevantes do formulário */
    inputs:
      { var nameInput = inputs.leadName = inputs.find( input => input.name == "header-name" ), /* Campo para inserção do nome */
            emailInput = inputs.leadEmail = inputs.find( input => input.name == "header-email" ) /* Campo para inserção do e-mail */ }
    var textArea = headerForm.textArea = headerForm.querySelector( "[ name='header-message' ]" ), /* Campo para inserção de mensagem */
        buttonsSet = headerForm.buttonsSet = headerForm.querySelector( ".buttons-set" ); /* Conjunto de botões */
    buttonsSet:
      { var submitButton = buttonsSet.submitButton = buttonsSet.querySelector( "[ type='submit' ]" ) /* Botão de submissão */ }
    var bottomContainer = headerForm.bottomContainer = headerForm.querySelector( ".bottom-container" ), /* Recipiente da parte inferior do formulário, somente usado na versão responsiva */
        formBackface = headerForm.backface = headerForm.parentElement.querySelector( ".form-backface" ); /* Face do formulário com notificação de submissão bem-sucedida */
    formBackface:
      { var successButton = formBackface.successButton = formBackface.querySelector( "[ name='header-success' ]" ) } }

/* Instruções Iniciais */
addToPlaceholders: // Adiciona asterisco ao fim do conteúdo de placeholders de campos obrigatórios do formulário
  { headerForm.oPlaceAsterisc() }

responsiveConfig: // Altera composição do formulário em telas de pouca largura
  { let targetMedia = topHeader.classList.contains( "profile" ) ? media.maxWidth[0] : media.maxWidth[1];
    agroupInputs: // Ajunta os inputs do formulário em uma única div
      { nameInput.oChangePlacementBySize( { innerWidth: targetMedia }, emailInput.parentElement, emailInput );
        window.addEventListener( "resize", () => nameInput.oChangePlacementBySize( { innerWidth: targetMedia }, emailInput.parentElement, emailInput ) ) }
    bottomContainer: // Cria uma div para que agrupe o textarea e o grupo de botões
      { for( let element of [ textArea, buttonsSet ] )
          { element.oChangePlacementBySize( { innerWidth: targetMedia }, bottomContainer );
            window.addEventListener( "resize", () => element.oChangePlacementBySize( { innerWidth: targetMedia }, bottomContainer ) ) } } }

successfulSubmission: // Eventos a acontecerem após uma submissão bem-sucedida
  { headerForm.addEventListener( "submit", event => event.preventDefault() ); // Impedir que formulário recarregue por padrão a página
    changeToBackface: // Altera face do formulário para a que informa seu preenchimento bem-sucedido
      { headerForm.addEventListener( "submit", () =>
          { headerForm.oControlAnimation(); successButton.clicked = false } );
        headerForm.addEventListener( "animationiteration", () =>
          { headerForm.oControlAnimation();
            if( !successButton.clicked ) formBackface.oControlAnimation() } );
        formBackface.addEventListener( "animationiteration", () =>
          { formBackface.oControlAnimation();
            if( successButton.clicked ) headerForm.oControlAnimation() } );
        successButton.addEventListener( "click", () =>
          { formBackface.oControlAnimation(); successButton.clicked = true } ) }
    resetInputFields: // Redefinir campos de input quando se estiver a voltar para a face frontal do formulário
      { successButton.addEventListener( "click", () => headerForm.oResetFields() ) } }
