/* Identificadores */

export const topHeader = document.getElementById( "top-header" ); /* Cabeçalho Superior */
topHeader:
  { var navBar = topHeader.querySelector( ".nav-container" ); /* Menu de Navegação */
    navBar:
      { var navContainer = navBar.container = navBar.querySelector( ".content-container" ), /* Recipiente de Conteúdo do Menu de Navegação */
            pagesList = navBar.pagesList = navBar.querySelector( ".pages-list" ) /* Menu de Páginas do Site */ } }

export const media = { maxWidth: [ 1079, 1119 ] };

/* Ouvintes de Evento */

slidingMenu: // Implementa menu deslizante para versão responsiva
  { let targetMedia = topHeader.classList.contains( "profile" ) ? media.maxWidth[0] : media.maxWidth[1];
    adjustHeight: // Em telas de pouca largura, torna altura padrão da lista de páginas igual a de seu primeiro elemento
      { for( let _event of [ "load", "resize" ] )
          { window.addEventListener( _event, () =>
            pagesList.oChangeBySize( { innerWidth: targetMedia },
                                     pagesList.oFlatSize.bind( pagesList, pagesList.firstElementChild, false ), () => pagesList.style.height = "" ) ) } }
    triggerSliding: // Em telas de pouca largura, ativa o deslizamento do menu
      { for( let _event of [ "load", "resize" ] )
          { window.addEventListener( _event, () =>
              { pagesList.oChangeBySize( { innerWidth: targetMedia },
                                         () => { pagesList.setAttribute( "tabindex", 0 ) }, () => { pagesList.removeAttribute( "tabindex" ) } ) } ) };
        window.addEventListener( "resize", () => { if( window.innerWidth > targetMedia ) pagesList.classList.remove( "slided" ) } );
        pagesList.addEventListener( "click", () =>
          { if( window.innerWidth > targetMedia ) return;
            pagesList.classList.toggle( "slided" ) ? pagesList.oEncompassChildren( false ) : pagesList.oFlatSize( pagesList.firstElementChild, false ) } )
        pagesList.addEventListener( "blur", () =>
          { if( pagesList.classList.contains( "slided" ) ) pagesList.click() } ) }
    adjustImgPlacement: // Ajustar posicionamento da imagem lateral do menu ao deslizamento da lista de páginas
      { pagesList.addEventListener( "click", () =>
          { if( window.innerWidth > targetMedia ) return;
            if( pagesList.classList.contains( "slided" ) ) navContainer.style.alignItems = "flex-start" } );
        pagesList.addEventListener( "transitionend", event =>
          { if( window.innerWidth > targetMedia || event.propertyName != "height" ) return;
            if( !pagesList.classList.contains( "slided" ) ) navContainer.style.alignItems = "" } ) } }
