/* Identificadores */

const overview = document.getElementById( "overview" ); // Seção de Dados Gerais de Licença
overview:
  { var dataList = overview.dataList = overview.querySelector( ".overview-data" );
    dataList:
      { var dataSets = dataList.dataSets = Array.from( dataList.getElementsByClassName( "data-set" ) ) } }

/* Ouvintes de Evento */

equalizeSetsHeight: // Tornar Igual a Altura dos Conjuntos de Data
  { window.addEventListener( "load", () =>
      { let tallestSet = dataSets.oFindTallest();
        for( let set of dataSets ) set.oFlatSize( tallestSet, false ) } ) }
