/* Identificadores */

const company = document.getElementById( "company" ); // Seção de Dados Gerais de Licença
company:
  { var dataList = company.dataList = company.querySelector( ".company-data" );
    dataList:
      { var dataSets = dataList.dataSets = Array.from( dataList.getElementsByClassName( "data-set" ) ) } }

/* Ouvintes de Evento */

equalizeSetsHeight: // Tornar Igual a Altura dos Conjuntos de Data
  { window.addEventListener( "load", () =>
      { let targetDataSets = dataSets.slice( 0, 6 ), tallestSet = targetDataSets.oFindTallest();
        for( let set of targetDataSets ) set.oFlatSize( tallestSet, false ) } ) }
