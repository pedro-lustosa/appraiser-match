/* Identificadores */

const feeSchedule = document.getElementById( 'fee-schedule' ); // Seção de Tabela de Tarifas
feeSchedule:
  { var displayerMenu = feeSchedule.displayerMenu = feeSchedule.querySelector( ".display-options" ); /* Menu de exibição dos dados da tabela */
    displayerMenu:
      { var menuOptions = displayerMenu.options = displayerMenu.querySelectorAll( "li" ) /* Opções do menu */ }
    var table = feeSchedule.table = feeSchedule.querySelector( ".schedule-table" ); /* Tabela de Tarifas */
    table:
      { var tData = table.data = table.querySelectorAll( "thead th" ); /* Células de Cabeçalho */
        data:
          { var fee = tData.fee = table.querySelectorAll( "tbody td:nth-of-type( 3 )" ), /* Valor da Tarifa */
                turnaroundTime = tData.turnaroundTime = table.querySelectorAll( "tbody td:nth-of-type( 4 )" ); /* Tempo de Resposta */
            turnaroundTime:
              { var turnaroundHeading = turnaroundTime.heading = tData[ 3 ] /* Cabeçalho da Coluna de Tempo de Resposta */ } } } }

const media = { maxWidth: [ 1119, 799 ] };

/* Métodos */

for( let td of fee )
  { td.compact = function()
      { if( this.responsiveText ) return this.responsiveText;
        var price = this.textContent.oFilterNumber();
        return this.responsiveText = `$${ price }+` } }

for( let td of turnaroundTime )
    { td.compact = function()
        { if( this.responsiveText ) return this.responsiveText;
          var day = this.textContent.oFilterNumber();
          return this.responsiveText = `<= ${ day }D` } }

turnaroundHeading.compact = function()
  { if( this.responsiveText ) return this.responsiveText;
    return this.responsiveText = "TAT" }

/* Eventos */

selectOptions: // Controla exibição de dados da tabela a partir da seleção das opções do menu
  { setCurrentOption: // Estiliza a opção do menu de que a tabela no momento está exibindo os dados
      { for( let option of menuOptions ) option.addEventListener( "click", () => option.oRestrictClass( "current", menuOptions, true ) ) } }

compactText: // Tornar textos da tabela mais resumidos para versões responsivas
  { tbody:
      { for( let target of [ fee, turnaroundTime ] )
          { for( let td of target )
              { td.oChangeBySize( { innerWidth: media.maxWidth[0] }, td.oChangeText.bind( td, td.compact() ) );
                window.addEventListener( "resize", () =>
                  { td.oChangeBySize( { innerWidth: media.maxWidth[0] },
                                        td.oChangeText.bind( td, td.compact() ), td.oChangeText.bind( td, td.formerText || td.textContent ) ) } ) } } }
    thead:
      { let heading = turnaroundHeading;
        heading.oChangeBySize( { innerWidth: media.maxWidth[1] }, heading.oChangeText.bind( heading, heading.compact() ) );
        window.addEventListener( "resize", () => heading.oChangeBySize( { innerWidth: media.maxWidth[1] },
                                                 heading.oChangeText.bind( heading, heading.compact() ), heading.oChangeText.bind( heading, heading.formerText || heading.textContent ) ) ) } }
