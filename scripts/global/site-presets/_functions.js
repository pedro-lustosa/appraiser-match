Object.defineProperties( Array.prototype,
  { oFindTallest: // Encontra maior elemento em uma relação
      { value: function oFindTallest()
          { if( this.some( item => !( item instanceof HTMLElement ) ) ) throw new TypeError( "The target array must contain only instances of HTMLElement" );
            return this.slice().sort( ( a, b ) => b.offsetHeight - a.offsetHeight )[0] },
        enumerable: true } } );

Object.defineProperties( HTMLFormElement.prototype,
  { oPlaceAsterisc: // Adiciona um asterisco ao placeholder de campos obrigatórios de formulários
      { value: function oPlaceAsterisc()
          { let requiredInputs = Array.from( this.elements ).filter( input => input.placeholder && input.required );
            for( let input of requiredInputs ) input.placeholder += " *" },
        enumerable: true },
    oResetFields: // Simula uma redefinição dos campos de formulários
      { value: function oResetFields()
          { var inputsFields = Array.from( this.elements ).filter( input => input.tagName != "BUTTON" );
            for( let input of inputsFields ) input.value = "" },
        enumerable: true } } );
