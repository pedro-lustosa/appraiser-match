/* Identificadores */

const topFooter = document.getElementById( "top-footer" ); // Rodapé Superior
topFooter:
  { var contactForm = topFooter.contactForm = topFooter.querySelector( "form[ name='footer-contact' ]" ) } /* Formulário de Contato Principal */

/* Instruções Iniciais */

addToPlaceholders: // Adiciona asterisco ao fim do conteúdo de placeholders de campos obrigatórios do formulário
  { contactForm.oPlaceAsterisc() }
