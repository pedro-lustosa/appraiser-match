"use strict";

/* Validações */
export const oPrimitiveToArray = function( evalued, primitive )
  { if( !Array.isArray( evalued ) ) evalued = typeof evalued == primitive ? Array.of( evalued ) : Array.from( evalued );
    return evalued }

export const oNodeToArray = function( evalued, shallow = true )
  { if( !Array.isArray( evalued ) ) evalued = evalued instanceof Node ? Array.of( evalued ) : Array.from( evalued )
    else if( shallow ) evalued = evalued.slice();
    return evalued }

/* Métodos de Textos */
upperAndLowerChars:
  { Object.defineProperties( String,
      { oUpperChars: { value: [], enumerable: true }, oLowerChars: { value: [], enumerable: true } } );
    let charsMap = { oUpperChars: 65, oLowerChars: 97 };
    for( let chars in charsMap )
      { for( let i = charsMap[ chars ], range = charsMap[ chars ] + 25; i < range; i++ )
        { String[ chars ].push( String.fromCharCode( i ) ) } } }

numbers:
  { Object.defineProperty( String, "oNumbers", { value: [], enumerable: true } );
    for( let i = 48; i < 58; i++ ) String.oNumbers.push( String.fromCharCode( i ) ) }

Object.defineProperties( String.prototype,
  { oCount:
      { value: function oCount( charList )
          { var charArray = oPrimitiveToArray( charList, "string" ), amount = [];
            for( let char of charArray )
              { let counter = 0;
                for( let i = 0; i < this.length; i++ ) if( this[ i ] == char ) counter++;
                amount.push( counter ) }
            return amount.length == 1 ? amount.pop() : amout },
        enumerable: true },
    oFindFirstChar: // Retorna o primeiro caractere de dado arranjo encontrado no alvo, ou o primeiro que não seja deste dado arranjo
      { value: function oFindFirstChar( charset, exclusive = false )
          { if( !Array.isArray( charset ) ) throw new TypeError( "The charset argument of oFindFirstChar must be an array." );
            if( charset.some( char => typeof char !== "string" ) ) throw new TypeError( "The values of charset array must be all strings." );
            if( typeof exclusive !== "boolean" ) throw new TypeError( "The exclusive argument of oFindFirstChar must be a boolean." );
            var actualChar = this.charAt( 0 ), includeExpression = exclusive ? "charset.includes( actualChar )" : "!charset.includes( actualChar )";
            for( let i = 1; actualChar && eval( includeExpression ); i++ ) actualChar = this.charAt( i );
            return actualChar },
        enumerable: true },
    oFindFirstIndex: // Retorna o número de índice do primeiro caractere de dado arranjo encontrado no alvo, ou do primeiro que não seja deste dado arranjo
      { value: function oFindFirstIndex( charset, exclusive = false )
          { var targetChar = this.oFindFirstChar( charset, exclusive );
            return targetChar ? this.indexOf( targetChar ) : -1 },
        enumerable: true },
    oInsertExcerpt:
      { value: function oInsertExcerpt( string, start, reverse = false )
          { if( typeof string !== "string" || typeof start !== "string" ) throw new TypeError( "The arguments 'string' and 'start' of oInsertExcerpt must be strings." );
            if( typeof reverse !== "boolean" ) throw new TypeError( "The argument 'reverse' of oInsertExcerpt must be a boolean." );
            if( !this.includes( start ) ) return false;
            var indexMethod = reverse ? "lastIndexOf" : "indexOf";
            return this.slice( 0, this[ indexMethod ]( start ) + start.length ) + string + this.slice( this[ indexMethod ]( start ) + start.length ) },
        enumerable: true },
    oBringExcerpt: // Retorna o trecho contido no alvo entre demarcadores de início e fim
      { value: function oBringExcerpt( start, end = "", exclusive = false )
          { if( typeof start !== "string" || typeof end !== "string" ) throw  new TypeError( "The arguments 'start' and 'end' of oBringExcerpt must be strings." );
            if( typeof exclusive !== "boolean" ) throw new TypeError( "The exclusive argument of oBringExcerpt must be a boolean." );
            var excerpt = this.slice( this.indexOf( start ) );
            excerpt = excerpt.slice( 0, excerpt.indexOf( end ) || excerpt.length );
            return exclusive ? excerpt.slice( start.length ) : excerpt },
        enumerable: true },
    oRemoveExcerpt:
      { value: function oRemoveExcerpt( start, end = "", reverse = false )
          { if( typeof start !== "string" || typeof end !== "string" ) throw new TypeError( "The arguments 'start' and 'end' of oRemoveExcerpt must be strings." );
            if( typeof reverse !== "boolean" ) throw new TypeError( "The argument 'reverse' of oRemoveExcerpt must be a boolean." );
            if( !this.includes( start ) || ( end && !this.includes( end ) ) ) return false;
            var indexMethod = reverse ? "lastIndexOf" : "indexOf";
            return this.slice( 0, this[ indexMethod ]( start ) ) + this.slice( this[ indexMethod ]( end ) + end.length ) },
        enumerable: true },
    oReplaceExcerpt:
      { value: function oReplaceExcerpt( string, target, reverse = false )
          { if( typeof string !== "string" || typeof target !== "string" ) throw new TypeError( "The arguments 'string' and 'target' of oReplaceExcerpt must be strings." );
            if( typeof reverse !== "boolean" ) throw new TypeError( "The argument 'reverse' of oReplaceExcerpt must be a boolean." );
            if( !this.includes( target ) ) return false;
            var indexMethod = reverse ? "lastIndexOf" : "indexOf";
            return this.slice( 0, this[ indexMethod ]( target ) ) + string + this.slice( this[ indexMethod ]( target ) + target.length ) },
        enumerable: true },
    oInvert:
      { value: function oInvert( string, reverse = false )
          { if( typeof string !== "string" ) throw new TypeError( "The argument string of oInvert must be a string." );
            if( typeof reverse !== "boolean" ) throw new TypeError( "The argument reverse of oInvert must be a boolean." );
            if( !string.includes( this ) ) return "";
            return reverse ?
              string.slice( 0, string.lastIndexOf( this ) ) + string.slice( string.lastIndexOf( this ) + this.length ) :
              string.replace( this, "" ) },
        enumerable: true },
    oRemoveChars:
      { value: function oRemoveChars( charList, count = Infinity, reverse = false )
          { if( typeof count !== "number" ) throw new TypeError( "The argument count of oRemoveChars must be a number." );
            if( count < 0 ) throw new RangeError( "The argument count of oRemoveChars must be greater than or equal 0." );
            if( typeof reverse !== "boolean" ) throw new TypeError( "The argument reverse of oRemoveChars must be a boolean." );
            var charArray = oPrimitiveToArray( charList, "string" ), newString = this;
            for( let char of charArray )
              { for( let i = 0; i < count; i++ )
                  { if( !newString.includes( char ) ) break;
                    newString = reverse ?
                      newString.slice( 0, newString.lastIndexOf( char ) ) + newString.slice( newString.lastIndexOf( char ) + 1 ) :
                      newString.replace( char, "" ) } }
            return newString },
        enumerable: true },
    oRemoveRepeatedChars:
      { value: function oRemoveRepeatedChars( charList, spare = 1, reverse = false )
          { if( typeof spare !== "number" ) throw new TypeError( "The argument spare of oRemoveRepeatedChars must be a number." );
            if( spare < 0 ) throw new RangeError( "The argument spare of oRemoveRepeatedChars must be greater or equal to 0." );
            if( typeof reverse !== "boolean" ) throw new TypeError( "The argument reverse of oRemoveChars must be a boolean." );
            if( spare == 0 ) return oRemoveChars( charList );
            var charArray = oPrimitiveToArray( charList, "string" ), fullString = this;
            for( let char of charArray )
              { if( !fullString.includes( char ) ) continue;
                let sparedExcerpt = reverse ? fullString.slice( 0, fullString.indexOf( char ) + char.length ) : fullString.slice( fullString.lastIndexOf( char ) ),
                    stringPart = reverse ? sparedExcerpt.oInvert( fullString ) : sparedExcerpt.oInvert( fullString, true );
                while( stringPart.includes( char ) && spare != sparedExcerpt.oCount( char ) )
                  { sparedExcerpt = reverse ?
                      sparedExcerpt + stringPart.slice( 0, stringPart.indexOf( char ) + char.length ) :
                      stringPart.slice( stringPart.lastIndexOf( char ) ) + sparedExcerpt;
                    stringPart = reverse ? sparedExcerpt.oInvert( fullString ) : sparedExcerpt.oInvert( fullString, true ) }
                stringPart = stringPart.oRemoveChars( char );
                fullString = reverse ? sparedExcerpt + stringPart : stringPart + sparedExcerpt }
            return fullString },
        enumerable: true },
    oFilterNumber: // Retorna o primeiro número ou agrupamento numérico do alvo
      { value: function oFilterNumber( decimalSeparator = ".", overlookedChars = [] )
          { if( typeof decimalSeparator !== "string" ) throw new TypeError( "The argument decimalSeparator of oFilterNumber must be a string." );
            if( !Array.isArray( overlookedChars ) ) throw new TypeError( "The argument overlookedChars of oFilterNumber must be an array." );
            if( overlookedChars.includes( decimalSeparator ) || overlookedChars.some( char => String.oNumbers.includes( char ) ) ) throw new RangeError( "The argument overlookedChars of oFilterNumber must not include the decimalSeparator value, or numeric values." );
            if( overlookedChars.some( char => typeof char !== "string" ) ) throw new TypeError( "The values of overlookedChars array must be all strings." );
            if( !String.oNumbers.some( number => this.includes( number ) ) ) return "";
            var numberSet = String.oNumbers.concat( decimalSeparator || [] ), totalSet = numberSet.concat( overlookedChars ),
                numericExcerpt = this.slice( this.oFindFirstIndex( numberSet ) );
            while( numericExcerpt[0] == decimalSeparator && !String.oNumbers.includes( numericExcerpt[1] ) )
              { numericExcerpt = numericExcerpt.slice( numericExcerpt.slice( 1 ).oFindFirstIndex( numberSet ) + 1 ) }
            if( !numericExcerpt.split("").every( char => numberSet.includes( char ) ) ) numericExcerpt = numericExcerpt.slice( 0, numericExcerpt.oFindFirstIndex( totalSet, true ) );
            for( let char of overlookedChars )
              { while( numericExcerpt.includes( char ) ) numericExcerpt = numericExcerpt.replace( char, "" ) }
            filterAdditionalSeparators:
              { let separatorIndex = numericExcerpt.indexOf( decimalSeparator );
                while( decimalSeparator && separatorIndex != numericExcerpt.lastIndexOf( decimalSeparator ) )
                  { numericExcerpt = numericExcerpt.slice( 0, separatorIndex + 1 ) + numericExcerpt.slice( separatorIndex + 1 ).replace( decimalSeparator, "" ) } }
            numericExcerpt = numericExcerpt.replace( decimalSeparator, "." );
            return Number( numericExcerpt ) },
        enumerable: true } } );

/* Métodos de Arranjos */
Object.defineProperties( Array.prototype,
  { oSingle: // Elimina valores repetidos do arranjo
      { value: function oSingle()
          { return this.filter( ( object, index, array ) => array.indexOf( object ) == index ) },
        enumerable: true },
    oTrim: // Elimina valores de um arranjo até o primeiro que atenda à avaliação da função de parâmetro
      { value: function oTrim( evaluer )
          { return this.slice( this.findIndex( value => evaluer( value ) ) || this.length ) },
        enumerable: true },
    oSegment: // Divide o arranjo em sub-arranjos, a começarem com o valor que atender à avaliação da função de parâmetro
      { value: function oSegment( evaluer, trim = false )
          { var newArray = [], startIndex = 0;
            this.forEach( ( value, index, array ) =>
              { if( index && evaluer( value, index, array ) ) { newArray.push( array.slice( startIndex, index ) ); startIndex = index } } );
            newArray.push( this.slice( startIndex ) );
            if( trim && !evaluer( newArray[0][0] ) ) newArray.shift();
            return newArray },
        enumerable: true },
    oSegmentByEnd: // Divide o arranjo em sub-arranjos, a terminarem com o valor que atender à avaliação da função de parâmetro
      { value: function oSegmentByEnd( evaluer, trim = false )
          { var newArray = [], startIndex = 0;
            this.forEach( ( value, index, array ) =>
              { if( evaluer( value, index, array ) ) { newArray.push( array.slice( startIndex, index + 1 ) ); startIndex = index + 1 } } );
            if( !trim && startIndex != this.length ) newArray.push( this.slice( startIndex ) );
            return newArray },
        enumerable: true },
    oCycle: // Quando um número não for abarcado pelo índice de um arranjo, decompõe seu valor até que o seja
      { value: function oCycle( number )
          { if( number >= this.length )
              { number = number % this.length }
            else if( number < 0 )
              { while( number < 0 ) number += this.length };
            return this[ number ] },
        enumerable: true },
    oSelectForEach: // Itera um arranjo de elementos para retornar um arranjo com os elementos abarcados pela dada string de seleção
      { value: function oSelectForEach( selector, target = "children", count = 1, range = Infinity )
          { if( !this.every( element => element instanceof Element ) ) throw new RangeError( "Just arrays with only elements are allowed by oSelectForEach." );
            if( typeof selector != "string" ) throw new TypeError( "The selector argument of oSelectForEach must be a string." );
            if( typeof count !== "number" || typeof range !== "number" ) throw new TypeError( "The count and range arguments of oSelectForEach must be numbers." );
            if( count <= 0 || range <= 0 ) throw new RangeError( "The count and range arguments of oSelectForEach must be greater than 0." );
            var selection = [], method;
            switch( target )
              { case "children": method = count > 1 ? "querySelectorAll" : "querySelector"; break;
                case "parents": method = "closest"; break;
                default: throw new RangeError( "The allowed values for target argument of oSelectForEach are 'children' and 'parents'." ) };
            switch( method )
              { case "querySelector": this.forEach( element =>
                  { selection = selection.concat( element.querySelector( selector ) || [] ) } ); break;
                case "querySelectorAll": this.forEach( element =>
                  { selection = selection.concat( Array.from( element.querySelectorAll( selector ) ).slice( 0, count ) ) } ); break;
                case "closest": this.forEach( element =>
                  { for( let actualElement = element.parentElement, i = 0; ( actualElement = actualElement.closest( selector ) ) && i < count; i++ )
                      { selection.push( actualElement ); actualElement = actualElement.parentElement } } ) }
            return selection },
        enumerable: true } } );

/* Métodos de Alvos de Eventos */
Object.defineProperties( EventTarget.prototype,
  { oAssignEvents: // Definir Valores de Propriedades de Eventos
      { value: function oAssignEvents( events )
          { for( let event in events ) this[ "on" + event ] = events[ event ] },
        enumerable: true },
    oCatchClicksWithKeys: // Acionar Cliques através de Teclagens
      { value: function oCatchClicksWithKeys( event, keys = [ "Enter" ] )
          { if( keys.some( key => event.key == key ) ) this.click() },
        enumerable: true } } );

/* Métodos da Janela */
  // Console
  Object.defineProperties( window.console,
    { oListLog: // Inseri em linhas separadas valores de variáveis no console
        { value: function oListLog()
            { var logString = "", lastExpression = arguments[ arguments.length - 1 ];
              for( let expression of arguments )
                { if( typeof expression !== 'string' ) throw new TypeError( 'All the arguments of oListLog must be strings.' )
                  logString += `${ expression }: ${ eval( expression ) }`;
                  if( expression != lastExpression ) logString += "\n" }
              return console.log( logString ) },
          enumerable: true },
      oEvaluePerformance: // Aferi tempo levado na execução de funções dado número de vezes
        { value: function oEvaluePerformance( functions )
            { if( typeof functions !== 'object' ) throw new TypeError( "The 'functions' argument of oEvalueTime must be an object, with, as keys, numbers indicating how many times given values are to be iterated, and with, as values, arrays of functions to evalue." );
              var measurementsSet = [];
              for( let timesCounter in functions )
                { if( Object.is( Number( timesCounter ), NaN ) ) throw new TypeError( "The keys of 'functions' argument of oEvalueTime must be numbers." );
                  if( timesCounter <= 0 ) throw new RangeError( "The keys of 'functions' argument of oEvalueTime must be greater than 0." );
                  if( !Array.isArray( functions[ timesCounter ] ) ) throw new TypeError( "The value or values of the 'functions' argument of oEvalueTime must be passed as an array." );
                  for( let _function of functions[ timesCounter ] )
                    { if( typeof _function !== "function" ) throw new TypeError( "The arrays values of 'function' argument of oEvalueTime must be all functions." );
                      let startMark = _function.name + "Start", endMark = _function.name + "End",
                          measurement = _function.name;
                      performance.mark( startMark );
                      for( let i = 0; i < timesCounter; i++ ) _function();
                      performance.mark( endMark );
                      performance.measure( measurement, startMark, endMark );
                      let actualMeasurement = performance.getEntriesByName( measurement ).oCycle( -1 );
                      actualMeasurement.timesCounter = timesCounter; measurementsSet.push( actualMeasurement );
                      performance.clearMarks( startMark ); performance.clearMarks( endMark ) } }
              return measurementsSet },
          enumerable: true },
      oListPerformance:
        { value: function oListPerformance( functions, divisor = 0 )
            { var measurementsSet = window.console.oEvaluePerformance( functions ), logString = "", count = 0;
              for( let measurement of measurementsSet )
                { logString += `${ ++count }. ${ measurement.name }: ${ measurement.duration / 10 ** divisor } (Executed ${ measurement.timesCounter } times)\n` }
              return console.log( logString.trimEnd() ) },
          enumerable: true },
      oContrastPerformance:
        { value: function oContrastPerformance( functions, divisor = 0 )
            { var measurementsSet = Array.from( window.console.oEvaluePerformance( functions ) ).oSegmentByEnd( ( measure, index, array ) => measure.timesCounter != array.oCycle( index + 1 ).timesCounter ),
                  logString = "", count = 0, timeUnit;
              if( measurementsSet.some( timesCounterSet => timesCounterSet.length < 2 ) )
                { console.warn( "Keys with only one function were ignored during oContrastPerformance evaluation." );
                  measurementsSet = measurementsSet.filter( timesCounterSet => timesCounterSet.length >= 2 ) }
              switch( divisor )
                { case 0: timeUnit = "ms"; break;
                  case 1: timeUnit = "cs"; break;
                  case 2: timeUnit = "ds"; break;
                  case 3: timeUnit = "s"; break;
                  default: throw new RangeError( "Divisor argument of oContrastPerformance must be a number between 0 and 3." ) }
              for( let timesCounterSet of measurementsSet )
                { timesCounterSet.sort( ( a, b ) => a.duration - b.duration );
                  for( let measurement of timesCounterSet )
                    { if( measurement == timesCounterSet[0] ) { logString += `${ ++count }. ${ measurement.name } is:\n    `; continue }
                      logString += `${ ( measurement.duration - timesCounterSet[0].duration ) / 10 ** divisor }${ timeUnit } faster than ${ measurement.name }`;
                      logString += measurement == timesCounterSet.oCycle( -1 ) ? `. (Functions executed ${ measurement.timesCounter } times)${ "\n".repeat( 2 ) }` : ", " } }
              return console.log( logString.trimEnd() ) },
          enumerable: true } } );

/* Métodos de Nós */
Object.defineProperties( Node.prototype,
  { oChangeBySize: // Executa uma função ou retorna um boolean segundo o tamanho de dado objeto
      { value: function oChangeBySize( media, truthFunction, falsyFunction, evaluedNode )
          { var targetComponent;
            let acceptedMediaKeys = [ [ "innerWidth", "innerHeight", "outerWidth", "outerHeight" ],
                                      [ "width", "height", "availWidth", "availHeight" ],
                                      [ "clientWidth", "clientHeight", "offsetWidth", "offsetHeight", "scrollWidth", "scrollHeight" ] ];
            for( let size in media )
              { if( typeof media[ size ] !== "number" ) throw new TypeError( "Object value in media argument of oChangeBySize must be a number." );
                let sizeID;
                for( let i = 0; i < acceptedMediaKeys.length; i++ )
                  { if( acceptedMediaKeys[i].includes( size ) ) { sizeID = i; break } }
                switch( sizeID )
                  { case 0: targetComponent = window; break;
                    case 1: targetComponent = window.screen; break;
                    case 2: targetComponent = evaluedNode || this; break;
                    default: throw new RangeError( "Object key in media argument of oChangeBySize function must be a window, screen or element size property." ) }
                if( targetComponent[ size ] > media[ size ] ) return falsyFunction ? falsyFunction() : false }
            return truthFunction ? truthFunction() : true },
        enumerable: true },
    oChangePlacementBySize: // Realiza a inserção do alvo em um receptáculo em função do tamanho de dado objeto
      { value: function oChangePlacementBySize( media, receiver, nextSibling, evaluedNode )
          { let originalParent = this.originalParent = this.originalParent || this.parentElement,
                originalSibling = this.originalSibling = this.originalSibling || this.nextElementSibling || "none";
            if( this.oChangeBySize( media, false, false, evaluedNode || receiver ) )
              { if( !Array.from( receiver.children ).includes( this ) ) nextSibling ? receiver.insertBefore( this, nextSibling ) : receiver.appendChild( this ) }
            else
              { if( Array.from( receiver.children ).includes( this ) )
                  { originalSibling != "none" && Array.from( originalParent.children ).includes( originalSibling ) ?
                      originalParent.insertBefore( this, originalSibling ) : originalParent.appendChild( this ) } } },
        enumerable: true } } );

/* Métodos de Elementos */
Object.defineProperties( Element.prototype,
  { oFindDepth: // Retorna um número correspondente à profundidade do alvo na árvore do DOM
      { value: function oFindDepth( baseElement = document.documentElement, depth = 0 )
          { if( !baseElement.contains( this ) ) return;
            if( baseElement.isSameNode( this ) ) return depth;
            var elementChildren = baseElement.children;
            for( let child of elementChildren )
              { if( child.contains( this ) ) return this.oFindDepth( child, ++depth ) } },
        enumerable: true },
    oBringParents: // Retorna um arranjo com os elementos do primeiro argumento ascendentes do alvo, opcionalmente delimitados por quantidade e alcance
      { value: function oBringParents( elementList, count = Infinity, range = Infinity )
          { var actualParent = this.parentElement, targetParents = [];
            if( count <= 0 ) return [];
            if( range < 0 ) if( ( range = this.oFindDepth() + range ) < 0 ) return count == 1 ? undefined : [];
            var arrayList = oNodeToArray( elementList );
            parentsLoop: while( actualParent && arrayList.length && range )
              { while( arrayList.includes( actualParent ) )
                  { targetParents = targetParents.concat( arrayList.splice( arrayList.findIndex( element => element.isSameNode( actualParent ) ), 1 ) );
                    if( targetParents.length == count ) break parentsLoop }
                actualParent = actualParent.parentElement; range-- }
            return count == 1 ? targetParents.pop() : targetParents },
        enumerable: true },
    oBringChildren: // Retorna um arranjo com os elementos do primeiro argumento descendentes do alvo, opcionalmente delimitados por quantidade e alcance
      { value: function oBringChildren( elementList, count = Infinity, range = Infinity )
          { var actualChildren = this.children, targetChildren = [];
            if( !actualChildren.length || count <= 0 || range <= 0 ) return [];
            var arrayList = oNodeToArray( elementList );
            childrenLoop: for( let child of actualChildren )
              { while( arrayList.includes( child ) )
                  { targetChildren = targetChildren.concat( arrayList.splice( arrayList.findIndex( element => element.isSameNode( child ) ), 1 ) );
                    if( targetChildren.length == count ) break childrenLoop }
                if( range > 1 && arrayList.length ) targetChildren = targetChildren.concat( child.oBringChildren( arrayList, count - targetChildren.length, range - 1 ) || [] );
                if( targetChildren.length == count ) break }
            return count == 1 ? targetChildren.pop() : targetChildren },
        enumerable: true },
    oTriggerMediaLoading: // Habilita a possibilidade de carregamento de dado recurso midiático
      { value: function oTriggerMediaLoading()
          { switch( this.tagName )
              { case "PICTURE": for( let image of this.children ) checkDataset( image ); break;
                case "SOURCE":
                case "IMG": checkDataset( this ); break;
                case "VIDEO":
                case "AUDIO": checkDataset( this );
                  for( let source of this.children ) checkDataset( source ); break;
                default: throw new RangeError( "The target of oTriggerMediaLoading must be either a <picture>, <source>, <img>, <video> or <audio> element." ) }
            function checkDataset( element )
              { if( element.dataset.src )
                  { element.setAttribute( element.tagName == "SOURCE" && element.parentElement.tagName == "PICTURE" ? "srcset" : "src", element.dataset.src );
                    element.removeAttribute( "data-src" ) } } },
        enumerable: true },
    oRestrictClass: // Dentro de dado grupo, restringe uma classe de modo que apenas o alvo a tenha
        { value: function oRestrictClass( _class, elementsGroup, checkTarget = false )
            { if( typeof _class !== "string" ) throw new TypeError( "The '_class' argument of oRestrictClass must be a string." );
              if( checkTarget && this.classList.contains( _class ) ) return;
              if( oNodeToArray( elementsGroup ).some( element => !( element instanceof Element ) ) ) throw new TypeError( "The 'elementsGroup' argument of oRestrictClass must have only instances of Element." );
              for( let element of elementsGroup )
                { if( element.classList.contains( _class ) ) element.classList.remove( _class ) }
              this.classList.add( _class ) },
          enumerable: true },
    oChangeClassesBySize: // Alterna as classes do alvo em função do tamanho de tela
      { value: function oChangeClassesBySize( media, classes, evaluedNode )
          { for( let operator in classes )
              { classes[ operator ] = oPrimitiveToArray( classes[ operator ], "string" );
                switch( operator )
                  { case "add": for( let _class of classes[ operator ] ) this.classList.toggle( _class, this.oChangeBySize( media, false, false, evaluedNode ) ); break;
                    case "remove": for( let _class of classes[ operator ] ) this.classList.toggle( _class, !this.oChangeBySize( media, false, false, evaluedNode ) ); break;
                    default: throw new RangeError( "Object key in classes argument of oChangeClassesBySize function must be 'add' or 'remove'." ) } } },
        enumerable: true } } );

/* Métodos de Elementos do HTML */
Object.defineProperties( HTMLElement.prototype,
  { oMatchSize: // Redimensiona alvo em relação a dado elemento
      { value: function oMatchSize( element = this.previousElementSibling || this.nextElementSibling, targetSizes = { width: "scrollWidth", height: "scrollHeight" }, multiplier = 1 )
          { if( !( element instanceof HTMLElement ) ) throw new TypeError( "The element argument of oMatchSize must be a HTMLElement." );
            var allowedKeys = [ "minWidth", "minHeight", "width", "height", "maxWidth", "maxHeight" ], allowedValues = [ "clientWidth", "clientHeight", "offsetWidth", "offsetHeight", "scrollWidth", "scrollHeight" ];
            for( let size in targetSizes )
              { if( !allowedKeys.includes( size ) ) throw new RangeError( "Object keys in targetSizes argument of oMatchSize must be valid CSS properties related to width or height." );
                if( !allowedValues.includes( targetSizes[ size ] ) ) throw new RangeError( "Object value in targetSizes argument of oMatchSize must be an DOM element size property." );
                this.style[ size ] = this.oGetElementSize( size, targetSizes[ size ], element ) * multiplier + "px" } },
        enumerable: true },
    oFlatSize: // Equaliza medidas do alvo às de dado elemento
      { value: function oFlatSize( element = this.previousElementSibling || this.nextElementSibling, width = true, height = true )
          { if( width && height ) return this.oMatchSize( element, { width: "offsetWidth", height: "offsetHeight" }, 1 );
            if( width ) return this.oMatchSize( element, { width: "offsetWidth" }, 1 );
            if( height ) return this.oMatchSize( element, { height: "offsetHeight" }, 1 );
            throw new TypeError( "The function oFlatSize requires that at least one size measure be passed." ) },
        enumerable: true },
    oConstraintSize: // Limita medidas do alvo às de dado elemento
      { value: function oConstraintSize( element = this.previousElementSibling || this.nextElementSibling, width = true, height = true )
          { if( width && height ) return this.oMatchSize( element, { maxWidth: "offsetWidth", maxHeight: "offsetHeight" }, 1 );
            if( width ) return this.oMatchSize( element, { maxWidth: "offsetWidth" }, 1 );
            if( height ) return this.oMatchSize( element, { maxHeight: "offsetHeight" }, 1 );
            throw new TypeError( "The function oConstraintSize requires that at least one size measure be passed." ) },
        enumerable: true },
    oEncompassChildren: // Expande medidas do alvo até que alcance a total de seus descendentes
      { value: function oEncompassChildren( width = true, height = true )
          { for( let size of arguments )
              { if( typeof size !== "boolean" ) throw new TypeError( "oEncompassChildren arguments must be booleans; the first, to enable width resizing, the second, to enable height resizing." ) };
            if( width ) this.style.width = this.oGetElementSize( "width" ) + "px"; if( height ) this.style.height = this.oGetElementSize( "height" ) + "px" },
        enumerable: true },
    oGetElementSize: // Complemento para funções que pedem medidas do elemento
      { value: function oGetElementSize( size, targetSize, element = this )
          { var boxType = window.getComputedStyle( this ).boxSizing,
                scrollSize = "scroll" + ( size[ 0 ].toUpperCase() + size.slice( 1 ) ), offsetSize = "offset" + ( size[ 0 ].toUpperCase() + size.slice( 1 ) );
            return boxType == "border-box" ? element[ targetSize || scrollSize ] : element[ targetSize || scrollSize ] - ( element[ offsetSize ] - window.getComputedStyle( element )[ size ].oFilterNumber() ) },
        enumerable: true },
    oChangeText: // Altera texto do alvo, e armazena o original em uma propriedade
      { value: function oChangeText( textSource, textType = "textContent", update = false )
          { if( this[ textType ] == textSource ) return;
            if( typeof textSource !== "string" ) throw new TypeError( "Argument textSource of oChangeText must be a string." );
            let allowedProperties = [ "innerText", "outerText", "textContent", "innerHTML" ];
            if( !allowedProperties.includes( textType ) ) throw new RangeError( "Object value for textType argument of the function oChangeText must be an element text property." );
            if( !this[ 'formerText' ] || update ) this.formerText = this[ textType ];
            this[ textType ] = textSource },
        enumerable: true },
    oControlAnimation: // Controla o início e fim de animações
      { value: function oControlAnimation()
          { return this.style.animationPlayState = window.getComputedStyle( this ).animationPlayState == "paused" ? "running" : "paused" },
        enumerable: true } } );

/* Métodos de Elementos do HTML */
	/* DL */
  Object.defineProperties( HTMLDListElement.prototype,
    { oGetTerms: // Retorna todos os DT diretos da DL
        { value: function getTerms()
            { return Array.from( this.children ).filter( element => element.tagName == "DT" ) },
          enumerable: true },
      oGetDescriptions: // Retorna todos os DD diretos da DL
        { value: function getDescriptions()
            { return Array.from( this.children ).filter( element => element.tagName == "DD" ) },
          enumerable: true },
      oGetEntries: // Retorna arranjos encabeçados pelos DT diretos da DL e continuados por seus respectivos DD
        { value: function getEntries()
            { var dt = this.oGetTerms(), dlEntries = [];
              for( let i = 0; i < dt.length; i++ )
                { dlEntries.push( Array.of( dt[i] ) );
                  var targetElement = dlEntries[i][0].nextElementSibling;
                  while( targetElement && targetElement.tagName != "DT" )
                    { if( targetElement.tagName == "DD" ) dlEntries[i].push( targetElement );
                      targetElement = targetElement.nextElementSibling } };
              return dlEntries },
          enumerable: true } } );
