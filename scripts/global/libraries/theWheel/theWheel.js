"use strict";

/*  */
export const oNodeToArray = function( evalued, shallow = true )
  { if( !Array.isArray( evalued ) ) evalued = evalued instanceof Node ? Array.of( evalued ) : Array.from( evalued )
    else if( shallow ) evalued = evalued.slice();
    return evalued }

/* Métodos de Textos */
numbers:
  { Object.defineProperty( String, "oNumbers", { value: [], enumerable: true } );
    for( let i = 48; i < 58; i++ ) String.oNumbers.push( String.fromCharCode( i ) ) }

Object.defineProperties( String.prototype,
  { oFindFirstChar: // Retorna o primeiro caractere de dado arranjo encontrado no alvo, ou o primeiro que não seja deste dado arranjo
      { value: function oFindFirstChar( charset, exclusive = false )
          { if( !Array.isArray( charset ) ) throw new TypeError( "The charset argument of oFindFirstChar must be an array." );
            if( charset.some( char => typeof char !== "string" ) ) throw new TypeError( "The values of charset array must be all strings." );
            if( typeof exclusive !== "boolean" ) throw new TypeError( "The exclusive argument of oFindFirstChar must be a boolean." );
            var actualChar = this.charAt( 0 ), includeExpression = exclusive ? "charset.includes( actualChar )" : "!charset.includes( actualChar )";
            for( let i = 1; actualChar && eval( includeExpression ); i++ ) actualChar = this.charAt( i );
            return actualChar },
        enumerable: true },
    oFindFirstIndex: // Retorna o número de índice do primeiro caractere de dado arranjo encontrado no alvo, ou do primeiro que não seja deste dado arranjo
      { value: function oFindFirstIndex( charset, exclusive = false )
          { var targetChar = this.oFindFirstChar( charset, exclusive );
            return targetChar ? this.indexOf( targetChar ) : -1 },
        enumerable: true },
    oFilterNumber: // Retorna o primeiro número ou agrupamento numérico do alvo
      { value: function oFilterNumber( decimalSeparator = ".", overlookedChars = [] )
          { if( typeof decimalSeparator !== "string" ) throw new TypeError( "The argument decimalSeparator of oFilterNumber must be a string." );
            if( !Array.isArray( overlookedChars ) ) throw new TypeError( "The argument overlookedChars of oFilterNumber must be an array." );
            if( overlookedChars.includes( decimalSeparator ) || overlookedChars.some( char => String.oNumbers.includes( char ) ) ) throw new RangeError( "The argument overlookedChars of oFilterNumber must not include the decimalSeparator value, or numeric values." );
            if( overlookedChars.some( char => typeof char !== "string" ) ) throw new TypeError( "The values of overlookedChars array must be all strings." );
            if( !String.oNumbers.some( number => this.includes( number ) ) ) return "";
            var numberSet = String.oNumbers.concat( decimalSeparator || [] ), totalSet = numberSet.concat( overlookedChars ),
                numericExcerpt = this.slice( this.oFindFirstIndex( numberSet ) );
            while( numericExcerpt[0] == decimalSeparator && !String.oNumbers.includes( numericExcerpt[1] ) )
              { numericExcerpt = numericExcerpt.slice( numericExcerpt.slice( 1 ).oFindFirstIndex( numberSet ) + 1 ) }
            if( !numericExcerpt.split("").every( char => numberSet.includes( char ) ) ) numericExcerpt = numericExcerpt.slice( 0, numericExcerpt.oFindFirstIndex( totalSet, true ) );
            for( let char of overlookedChars )
              { while( numericExcerpt.includes( char ) ) numericExcerpt = numericExcerpt.replace( char, "" ) }
            filterAdditionalSeparators:
              { let separatorIndex = numericExcerpt.indexOf( decimalSeparator );
                while( decimalSeparator && separatorIndex != numericExcerpt.lastIndexOf( decimalSeparator ) )
                  { numericExcerpt = numericExcerpt.slice( 0, separatorIndex + 1 ) + numericExcerpt.slice( separatorIndex + 1 ).replace( decimalSeparator, "" ) } }
            numericExcerpt = numericExcerpt.replace( decimalSeparator, "." );
            return Number( numericExcerpt ) },
        enumerable: true } } );

/* Métodos de Arranjos */
Object.defineProperties( Array.prototype,
  { oCycle: // Quando um número não for abarcado pelo índice de um arranjo, decompõe seu valor até que o seja
    { value: function oCycle( number )
        { if( number >= this.length )
            { number = number % this.length }
          else if( number < 0 )
            { while( number < 0 ) number += this.length };
          return this[ number ] },
      enumerable: true } } );

/* Métodos de Nós */
Object.defineProperties( Node.prototype,
  { oChangeBySize: // Executa uma função ou retorna um boolean segundo o tamanho de dado objeto
      { value: function oChangeBySize( media, truthFunction, falsyFunction, evaluedNode )
          { var targetComponent;
            let acceptedMediaKeys = [ [ "innerWidth", "innerHeight", "outerWidth", "outerHeight" ],
                                      [ "width", "height", "availWidth", "availHeight" ],
                                      [ "clientWidth", "clientHeight", "offsetWidth", "offsetHeight", "scrollWidth", "scrollHeight" ] ];
            for( let size in media )
              { if( typeof media[ size ] !== "number" ) throw new TypeError( "Object value in media argument of oChangeBySize must be a number." );
                let sizeID;
                for( let i = 0; i < acceptedMediaKeys.length; i++ )
                  { if( acceptedMediaKeys[i].includes( size ) ) { sizeID = i; break } }
                switch( sizeID )
                  { case 0: targetComponent = window; break;
                    case 1: targetComponent = window.screen; break;
                    case 2: targetComponent = evaluedNode || this; break;
                    default: throw new RangeError( "Object key in media argument of oChangeBySize function must be a window, screen or element size property." ) }
                if( targetComponent[ size ] > media[ size ] ) return falsyFunction ? falsyFunction() : false }
            return truthFunction ? truthFunction() : true },
        enumerable: true },
    oChangePlacementBySize: // Realiza a inserção do alvo em um receptáculo em função do tamanho de dado objeto
      { value: function oChangePlacementBySize( media, receiver, nextSibling, evaluedNode )
          { let originalParent = this.originalParent = this.originalParent || this.parentElement,
                originalSibling = this.originalSibling = this.originalSibling || this.nextElementSibling || "none";
            if( this.oChangeBySize( media, false, false, evaluedNode || receiver ) )
              { if( !Array.from( receiver.children ).includes( this ) ) nextSibling ? receiver.insertBefore( this, nextSibling ) : receiver.appendChild( this ) }
            else
              { if( Array.from( receiver.children ).includes( this ) )
                  { originalSibling != "none" && Array.from( originalParent.children ).includes( originalSibling ) ?
                      originalParent.insertBefore( this, originalSibling ) : originalParent.appendChild( this ) } } },
        enumerable: true } } );

/* Métodos de Elementos */
Object.defineProperties( Element.prototype,
  { oRestrictClass: // Dentro de dado grupo, restringe uma classe de modo que apenas o alvo a tenha
      { value: function oRestrictClass( _class, elementsGroup, checkTarget = false )
          { if( typeof _class !== "string" ) throw new TypeError( "The '_class' argument of oRestrictClass must be a string." );
            if( checkTarget && this.classList.contains( _class ) ) return;
            if( oNodeToArray( elementsGroup ).some( element => !( element instanceof Element ) ) ) throw new TypeError( "The 'elementsGroup' argument of oRestrictClass must have only instances of Element." );
            for( let element of elementsGroup )
              { if( element.classList.contains( _class ) ) element.classList.remove( _class ) }
            this.classList.add( _class ) },
        enumerable: true } } );

/* Métodos de Elementos do HTML */
Object.defineProperties( HTMLElement.prototype,
  { oMatchSize: // Redimensiona alvo em relação a dado elemento
      { value: function oMatchSize( element = this.previousElementSibling || this.nextElementSibling, targetSizes = { width: "scrollWidth", height: "scrollHeight" }, multiplier = 1 )
          { if( !( element instanceof HTMLElement ) ) throw new TypeError( "The element argument of oMatchSize must be a HTMLElement." );
            var allowedKeys = [ "minWidth", "minHeight", "width", "height", "maxWidth", "maxHeight" ], allowedValues = [ "clientWidth", "clientHeight", "offsetWidth", "offsetHeight", "scrollWidth", "scrollHeight" ];
            for( let size in targetSizes )
              { if( !allowedKeys.includes( size ) ) throw new RangeError( "Object keys in targetSizes argument of oMatchSize must be valid CSS properties related to width or height." );
                if( !allowedValues.includes( targetSizes[ size ] ) ) throw new RangeError( "Object value in targetSizes argument of oMatchSize must be an DOM element size property." );
                this.style[ size ] = this.oGetElementSize( size, targetSizes[ size ], element ) * multiplier + "px" } },
        enumerable: true },
    oFlatSize: // Equaliza medidas do alvo às de dado elemento
      { value: function oFlatSize( element = this.previousElementSibling || this.nextElementSibling, width = true, height = true )
          { if( width && height ) return this.oMatchSize( element, { width: "offsetWidth", height: "offsetHeight" }, 1 );
            if( width ) return this.oMatchSize( element, { width: "offsetWidth" }, 1 );
            if( height ) return this.oMatchSize( element, { height: "offsetHeight" }, 1 );
            throw new TypeError( "The function oFlatSize requires that at least one size measure be passed." ) },
        enumerable: true },
    oEncompassChildren: // Expande medidas do alvo até que alcance a total de seus descendentes
      { value: function oEncompassChildren( width = true, height = true )
          { for( let size of arguments )
              { if( typeof size !== "boolean" ) throw new TypeError( "oEncompassChildren arguments must be booleans; the first, to enable width resizing, the second, to enable height resizing." ) };
            if( width ) this.style.width = this.oGetElementSize( "width" ) + "px"; if( height ) this.style.height = this.oGetElementSize( "height" ) + "px" },
        enumerable: true },
    oGetElementSize: // Complemento para funções que pedem medidas do elemento
      { value: function oGetElementSize( size, targetSize, element = this )
          { var boxType = window.getComputedStyle( this ).boxSizing,
                scrollSize = "scroll" + ( size[ 0 ].toUpperCase() + size.slice( 1 ) ), offsetSize = "offset" + ( size[ 0 ].toUpperCase() + size.slice( 1 ) );
            return boxType == "border-box" ? element[ targetSize || scrollSize ] : element[ targetSize || scrollSize ] - ( element[ offsetSize ] - window.getComputedStyle( element )[ size ].oFilterNumber() ) },
        enumerable: true },
    oChangeText: // Altera texto do alvo, e armazena o original em uma propriedade
      { value: function oChangeText( textSource, textType = "textContent", update = false )
          { if( this[ textType ] == textSource ) return;
            if( typeof textSource !== "string" ) throw new TypeError( "Argument textSource of oChangeText must be a string." );
            let allowedProperties = [ "innerText", "outerText", "textContent", "innerHTML" ];
            if( !allowedProperties.includes( textType ) ) throw new RangeError( "Object value for textType argument of the function oChangeText must be an element text property." );
            if( !this[ 'formerText' ] || update ) this.formerText = this[ textType ];
            this[ textType ] = textSource },
        enumerable: true },
    oControlAnimation: // Controla o início e fim de animações
        { value: function oControlAnimation()
            { return this.style.animationPlayState = window.getComputedStyle( this ).animationPlayState == "paused" ? "running" : "paused" },
          enumerable: true } } );
