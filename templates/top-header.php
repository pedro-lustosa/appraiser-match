<header id="top-header">
  <div class="nav-container">
    <div class="content-container">
      <picture class="icon">
        <img src="multimedia/images/extra-small/ss-realty-1.png" alt="">
      </picture>
      <div class="links-container">
        <nav class="pages-menu">
          <ul class="pages-list">
            <li class="about current"><a href="#">About</a></li>
            <li class="license"><a href="#license">License</a></li>
            <li class="schedule"><a href="#fee-schedule">Fee Schedule</a></li>
            <li class="payment"><a href="#overview">Payment</a></li>
            <li class="contact"><a href="#top-footer">Contact Me</a></li>
          </ul>
        </nav>
        <nav class="contact-menu">
          <address class="contact-info">
            <p class="message">
              <span class="action">Call Now</span>
              <span class="tel">800 630 1790</span>
            </p>
          </address>
          <ul class="social-list">
            <li><a class="facebook" href="#"></a></li>
            <li><a class="linkedin" href="#"></a></li>
            <li><a class="instagram" href="#"></a></li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
  <div class="display-heading">
    <div class="content-container">
      <div class="heading-container">
        <hgroup class="heading-set">
          <h1 class="title">Ken Flowers</h1>
          <h2 class="subtitle">Residential and Commercial Real Estate Appraiser</h2>
        </hgroup>
      </div>
      <div class="button-container">
        <button type="button" name="contact">Contact Me</button>
      </div>
    </div>
  </div>
</header>
